\documentclass[10pt]{article}
\input{res/preamb.tex}

%% TODO
%% - Citations
%% - Double check references.

\begin{document}
\maketitle

\begin{abstract}
  At the International Congress of Mathematicians in Paris in 1900,
  David Hilbert presented 23 problems he deemed the most important
  questions for the coming century. Hilbert's tenth problem was solved
  by Yuri Matiyasevich in 1970. We are conducting a formal
  verification of Matiyasevich's comprehensive proof using the
  interactive proof assistant software
  Isabelle%
  \footnote{%
    % TODO : Create the repo
    \url{https://gitlab.com/hilbert-10/eucys-18/}
  }.

  Each lemma was implemented individually and proven step by step. The
  simplest mathematical laws like the commutative property and
  associative property are explicitly part of the formalised proof. At
  the time of writing, the majority of results from number theory and
  substantial portions of the register machines essential for the
  proof have already been formalised and verified.

  Additional work is expected to result in full verification of
  Matiyasevich's famous MRDP theorem and the resulting verification of
  Hilbert's tenth problem. This would be the first formally verified
  Hilbert problem. This will bring one of the most important results
  from the 20th century into the methods of the 21st century.
\end{abstract}

\section{Introduction}
At the beginning of the 20th century, David Hilbert presented his
famous list of 23 mathematical problems for mathematicians in the
subsequent decades to analyse. Up until the present day, fourteen of
them have since been solved in full and six in part. Hilbert's tenth
problem has since been solved in full as well, although perhaps not in
the way Hilbert might have hoped. Hilbert formulated the problem as a
request for a general process for determining whether a Diophantine
equation is solvable. A Diophantine equation refers to a polynomial
equation with integer coefficients such that only integer solutions
are sought for the equation. In 1970, Yuri Matiyasevich was able to
show that a process of this type is impossible.

In reaching this result, Matiyasevich was able to cap off 30 years of
work by mathematicians from all over the world. In recognition of the
preliminary work by Robinson, Davis and Putnam, the result is referred
to as the MRDP theorem in general. The proof of non-existence can be
difficult to grasp because it uses many complicated formulas and
concepts from number theory.

During a visit to Bremen, Matiyasevich told our current project
advisor, Dierk Schleicher, of plans for a project to have his proof
formally verified by computer. Schleicher passed the idea on to a
working group consisting of eleven undergraduates (see Annex for names
and details). As mathematics students just getting started in our
studies, we were very interested in collaborating on a project with
one of the world's leading mathematicians right at the beginning of
our academic studies.

Our goal is to translate Matiyasevich's proof into the Isabelle
system. Isabelle is an interactive theorem proof assistant, meaning it
is a computer program that can help to verify mathematical proofs.
During this verification, each step of the proof is reduced to its
axioms—basic mathematical assumptions—and verified down to the
smallest detail in this manner. Upon successful completion of our
project, this would be the first Hilbert problem with a solution
formally verified by computer. The entire Isabelle community can
access this formalisation and use it in additional proofs.

\section{Procedure}
The project is divided into three phases. First it was necessary to
comprehend Matiyasevich's mathematical proof [?] and to learn how to
use Isabelle before we could begin the actual programming work. In
October 2017, Matiyasevich visited our campus and we talked with him
about the general structure of formalisation. Shortly afterwards, we
organised a two-day workshop during which we read and analyzed
individual chapters of the proof in small working groups in order to
then present the information to the group as a whole. In this way,
each member of the group was able to understand and, especially,
reconstruct their own section down to the smallest detail by the end
of the meeting.

Finally, we learned the ``ISAR'' Intelligible Semi--Automated Reasoning
language used in Isabelle. Documentation and online tutorials were
very helpful in this work. Abhik, who already had some initial
experience with Isabelle, also provided two introductory sessions for
the other participants. In addition, we met frequently in common group
meetings to work out the step-by-step implementation of the proof and
to help each other with bugs and problems.

The formalisation itself consisted of writing functional and
mathematical code. The scope of the formalisation requires working in
parallel, meaning that various parts of the code are written
simultaneously by various people. But working in parallel also creates
new problems. For example, Marco needed positional notation to
implement the register machine, but positional notation still required
final development work from Abhik. Another problem is combining the
various resulting versions of the source code into one coherent file.
As a result, we decided to use a version control system that made it
possible to create and work on several versions. Once the work in the
individual versions is completed, we are able to combine them into
standardised code that takes all of the changes into account.

We opted to use the git version control system. We created a project
on gitlab.com with access provided to all group members. In addition,
the proof has been split between various people: Abhik and Marco
concentrated on the formalisation of register machines, while Benedikt
worked on sections of chapter 3. This made it possible for us to
divide up the tasks and avoid duplicating work.

\section{Results}
Before we can present our results, it is necessary to analyse the
question posed by Hilbert's tenth problem and the relationship with
the MRDP theorem in greater detail. In addition, we provide an
overview of the argumentation in Matiyasevich's proof and a brief
overview of interactive theorem proofs before explaining in detail
which steps are already formalised in Isabelle in specific ways.

\subsection{Introduction to Hilbert's tenth problem}
\begin{quote}
  Given a Diophantine equation with any number of unknown quantities
  and with rational integral numerical coefficients: To devise a
  process according to which it can be determined in a finite number
  of operations whether the equation is solvable in rational integers.
\end{quote}
Using this formulation, Hilbert sought a process with a finite number
of steps, an algorithm in modern vernacular, that decides whether a
Diophantine equation is solvable%
\footnote{%
  At the time the problem was posed, the concept of an algorithm as we
  use it in computability theory was not in common use. The rigorous
  characterisation of an algorithm only came decades later.%
}. It is important to note that the algorithm here is not expected to
find a solution. The algorithm being sought merely needs to output a
yes (solvable) or no (unsolvable) answer to whether a specific
Diophantine equation is solvable within a finite number of operations.

A \emph{Diophantine equation} (more specifically: parametric Diophantine
equation) is an equation in the form
\begin{equation}
  D(a_1, \dots, a_n, x_1, \dots, x_m) = 0
\end{equation}
where $D(a_1, \dots, a_n, x_1, \dots, x_m)$ is a polynomial of
variables $x_1, \dots, x_m$ with integer coefficients and integer
parameters $a_1, \dots, a_n$. This means that only addition and
multiplication between coefficients, parameters and variables is
permitted [?, Chapter 1].

Furthermore, the representation of parametric Diophantine equations as
Diophantine sets is relevant in Matiyasevich's proof. For a parametric
Diophantine equation, these sets contain all of the parameter values
for which the equation is solvable with integers. A simple example is
the equation
\begin{equation}\label{eqn:example}
  x^2 - a = 0
\end{equation}
which is solvable in integer form only if $a$ is a square number.
Thus, the Diophantine set is
$\mathfrak M = \{0, 1, 4, 9, 16, \dots\}$. For all other values of the
parameter $a$, particularly all negative values, it is not possible to
solve the equation \eqref{eqn:example}, this means there exists no
$x \in \ZZ$ such that $x^2 - a = 0$. Hilbert's tenth problem is easy
to solve for this example: Obviously
$a \in \mathfrak M \iff \sqrt{a} \in \ZZ$, which a computer can
quickly verify.

The property being sought for the set $\mathfrak M$ his then whether
it is possible to test whether all given $a$ are an element of set
$\mathfrak M$. This is referred to as the computability of a set.
Thus, Hilbert's tenth problem asks: ``Are all Diophantine sets
decidable?''

It should be noted that, although Hilbert's tenth problem was posed
for the integer domain, it can be reduced to the domain of natural
numbers in the same question\footnote{%
  This is evident from Lagrange's four-square theorem, see Section
  1.2.1 from [?] for details.%
}. Accordingly, if not explicitly noted, we concerned ourselves only
with natural numbers in this project. All variables, parameters and
coefficients are generally not negative in the following content.

\subsection{Introduction to the MRDP theorem}

\subsubsection{Recursively enumerable sets}
One final concept that is essential for the MRDP theorem is that of
the recursively enumerable set. This type of set is characterised by
the property that a computer program would be capable of outputting
every single element of the set (and only elements of the set) if it
had enough time to do so. The set of positive powers of two is an
example of a recursively enumerable set. It is easy to write a program
that, starting at $i = 0$, outputs the number $2^i$ in each iteration
and adds one to $i$. This program will output every power of two at
some point and never anything else\footnote{%
  If the program takes one second for each number, the power of two
  $2^n$ with $n \in \NN$ will be output after $n$ seconds, i.e. in a
  finite amount of time.%
}.

This then makes it readily apparent that every Diophantine set is a
recursively enumerable set. In the example of the
equation~\eqref{eqn:example}, we could write a program that tests out
every combination of $x, a \in \ZZ$ and outputs the corresponding
value $a$ for whenever there is a solution. This program will never
end, but it would output all elements of $\mathfrak M$ (and only those
elements) eventually. The same is true for any Diophantine set, even
if its corresponding equation includes several unknowns
$x_1, \dots, x_n$. This is possible because the finite Cartesian
product of a countable set is countable.

The proof that the reverse is also true, i.e. that every recursively
enumerable set is also a Diophantine set and, as a result, that the
definitions of a wide variety of concepts for Diophantine and
recursively enumerable sets are equivalent, is known as a result of
Davis, Putnam, Robinson and Matiyasevich. The implementation of this
theorem with its formal proof is the goal of our project.

\begin{thm}[Matiyasevich-Robinson-Putnam-Davis (MRPD), 1970]
  Every recursively enumerable set is Diophantine.
\end{thm}

The procedure for proving this theorem is explained in the following
section 3.3. % TODO: this should become a \ref{...}.

The MRDP leads directly to the conclusion that Hilbert's tenth problem
is unsolvable because there are recursively enumerable sets that are
undecidable \footnote{%
  This is derived from the famous halting problem from Alan Turing.
  The construction of such a set is not very complex and you can refer
  to Section 3 for more information.%
}. This means there are also Diophantine sets for which it is
impossible to decide whether a number is an element of the set. As a
result, not all Diophantine sets are decidable by a universal
algorithm.

\subsubsection{Exponentiation is Diophantine}
Chapter 3 shows that exponentiation (meaning an equation in the form
$a = b^c$) can be represented in Diophantine form. In accordance with
the definition of Diophantine equations, this specifically is not
actually allowed—only adding and multiplying variables. Nevertheless,
Matiyasevich succeeded in showing that an exponential equation of this
type can be depicted as a Diophantine equation with the help of a
generalised and exponentially growing form of the Fibonacci sequence.
Representing the equation $a = b^c$, for example, requires a normal
polynomial with 20 variables stretching over several lines\footnote{%
  See [?] and Appendix~[appendix:polynom].%
}.

\subsubsection{Register machines}
Chapter 4 introduces a very simple computer model, which can be
simulated using mathematical formulas. These register machiens (also
called Minsky machines) are theoretical constructs consisting of a
finite number of \emph{states} $\mathrm S1 \dots \mathrm Sm$ and a
finite of \emph{registers} $\mathrm R1, \dots \mathrm Rn$. The
registers can store a natural number ($a$ is the input value):
%% FIGURE: Register machines

At any point in time, the machine is in exactly one of the states.
Similar to a Turing machine, every state can perform a specific
function and then transition to a subsequent state. In these state
changes, the possible instructions of a register machine either
increase or decrease the value of exactly one register by one.
Alternatively, the entire machine can come to a stop:
\begin{align}
  \mathrm Sk &: \mathrm Rl++; \mathrm Si \\
  \mathrm Sk &: \mathrm Rl--; \mathrm Si; \mathrm Sj \\
  \mathrm Sk &: \mathrm{HALT}
\end{align}
If the state $\mathrm Sk$ increases the value of register $\mathrm Rl$
by one ($\mathrm Rl++$), the register machine then enters the state
$\mathrm Si$. If, in state $\mathrm Sk$,the value of the register
$\mathrm Rl$ is to be decreased by one ($\mathrm Rl$) and this was
successful, the register machine then enters the state $\mathrm Si$.
If, however, the value of the register was already $0$, it cannot be
decreased further and the machine enters the state $\mathrm Sj$
instead. It is this branching in particular that makes register
machines Turing complete. Accordingly, the machines resulting from
this can be used to generate or accept recursively enumerable sets
(Turing-computable sets). A table containing the values of all
registers at all time steps and the active state (the instruction to
be performed)\footnote{%
  The cell $s_{k, t}$ has a value of 1 if the register machine is in
  the state $\mathrm Sk$ at the time $t$, otherwise it is 0.%
} for eachtime step is called the state table for the register machine
and can be visualised as follows [?, Section 4.3]:
%% FIGURE: protocol

This protocol starts chronologically at $t = 0$ on the right side and
finishes at $t = q$ on the left side. The reason for this convention
is the goal of showing all cells in one row of the protocol in a
single large number. If, for example, none of the register cells
contains a value greater than nine, it is possible to select ten as a
base and easily read out the number $r_l$ with figures
$r_{1, q}, \dots, r_{l, 0}$ using the decimal system. These numbers
make it possible to simulate the register machine in equations as in
Section 4.4.2 from~\ref{undef}.

This representation is the \emph{positional notation}
$r_l = \sum_{t = 0}^qr_{t, l}b^t$ using base $b$ requires
exponentiation by its very nature. Since it is possible to simulate
register machines of equations which can use the numbers $r_l$ and
$s_k$, we obtain a simulation through exponential equations. In
accordance with the results from Chapter 3, there is Diophantine
encoding for every exponential encoding. In this way, it is possible
to represent all of the operations of a register machine in
Diophantine form. Hence, all sets that can be accepted by the register
machine (recursively enumerable sets by definition) can be represented
in Diophantine form.

\subsection{Interactive theorem proving}

Interactive theorem provers are computer programs that check whether
an existing proof is truly free of errors and whether it works
seamlessly. During implementation, they await instructions from the
user regarding which statements are to be used for the individual
steps and then verify whether the step is logically correct. If
applicable, they attempt to fill in gaps by themselves using
previously implemented statements or to construct counterexamples.
Regardless, a distinction must be made between interactive theorem
provers and automatic theorem provers, which are used to attempt to
find new proofs.

The proof of Kepler's conjecture from Hales and Ferguson, published in
1998, is a very important result that was able to be verified with the
aid of an interactive theorem prover. The mathematical proof
encompasses more than 300 pages in addition to around 40,000 lines of
computer code with a total size of more than 3 GB. Despite the
importance of the proof, the extensive size of the proof meant it was
still not reviewed in full by hand even four years after publication;
that sort of review was not foreseeable in the near future. As a
result, Hales started Project FlysPecK to review the proof with the
aid of computers. Working with people from all over the world, it was
possible to verify the proof for the conjecture formally in August
2014. [?]

For our own implementation, we took Yuri Matiyasevich's recommendation
and opted to use the interactive theorem prover Isabelle. That
software is being developed through collaboration between the
Technical University of Munich and the University of Cambridge, but
now it also contains contributions from institutions and individuals
from all over the world. In addition, Isabelle has outstanding
documentation and a very general approach. Unlike many other
interactive theorem provers, Isabelle generally allows for the
implementation of any mathematical proof.

\subsection{Implementation in Isabelle}
In this section, we would like to showcase a few examples of our
Isabelle source code, specifically the key functions and proofs, and
provide some explanation of them. The most up-to-date version of our
project, particularly including any progress made after the
publication of this paper, is licensed under the GNU Public License
3.0 (GPL v3) and published on gitlab.com:
\url{https://gitlab.com/hilbert-10/eucys-18/}

\subsubsection{Third chapter}
For the third chapter, the individual intermediate steps have been
formalised—initially without proof—so that the five total people
responsible for the chapter could work on the individual sections
separately. Accordingly, the chapter was divided up so that every
group member was given responsibility for a specific section of the
formalisation. If not explicitly noted otherwise, the file for the
third chapter in the git repository contains only code written by
Benedikt. The text below also only contains code written by Benedikt.

The entire third chapter works with the sequence
\begin{equation}
  \alpha_b(0) = 0, \quad
  \alpha_b(1) = 1, \quad
  \alpha_b(n + 2) = b\alpha_b(n + 1) - \alpha_b(n).
\end{equation}
for $b \geq 2$. This is implemented in Isabelle in the following way:

\begin{verbatim}
fun alpha :: "nat => nat => int" where
    "alpha b 0 = 0"
  | "alpha b (Suc 0) = 1"
  | "alpha b (Suc (Suc n)) = (int b) * (alpha b (Suc n)) - (alpha b n)"
\end{verbatim}

Here, \texttt{fun alpha} introduces the definition of a function
alpha. This is followed by the method signature \texttt{nat => nat =>
  int}, which specifies that \texttt{alpha} expects two natural
numbers (\texttt{nat}) as arguments and returns an integer
(\texttt{int}). The definition of the function alpha comes after
\texttt{where}. This is recursive and exactly matches the definition
of [alpha\_def]: First the program checks whether the second argument
is zero or one, 1 in order to return zero or one directly in the basic
case. Otherwise, alpha recursively calls itself. The main result
(3.23).
\begin{equation}
  \forall b \geq 2 \forall k > 0 :
  \alpha_b(m) \equiv 0 \mod{\alpha_b(k)} \iff m \equiv 0 \mod{k}.
\end{equation}
of Section 3.4 of [?], which Benedikt implemented in full, can be
coded as follows when using the defined function alpha:
%% Isabelle code

The line fixes \texttt{b k m :: nat} means that $b, k, m$ can be any
natural number but are defined as natural numbers. In addition, the
next line requires that $b \geq 2$ and $k > 0$. The statement of the
theorem that is introduced with \texttt{shows} then corresponds to
[eq: 3.23] exactly. In the last line, \texttt{is [...]} introduces a
local abbreviation that can be accessed in the proof. This avoids
having to write out the statements explicitly with each use, which
helps preserve clarity. In this example, \texttt{?P} is an
abbreviation for the left side of the equivalence and \texttt{?Q} is
an abbreviation for the right side. The start of the implemented proof
has the following form:
%% Isabelle code

This example also provides an apt demonstration of the general
structure of an Isabelle proof: The first line (\texttt{proof})
introduces the proof. In the next line, the statement \texttt{?Q}
(right side) is initially assumed to be true. We introduce the
abbreviation \texttt{Q} for this assumption. Now it must be proven
that \texttt{Q} implies the left side \texttt{?P} of the equivalence.
First, \texttt{n} is defined, which is used frequently in the proof.
The next lines all have the form \texttt{from [...] have [name:] [...]
  by simp} very typical of Isabelle/ISAR. The previously indicated
assumptions are in the first blank. These can be steps that were shown
in the proof previously or lemmata/theorems that have already been
shown outside the proof. The second blank can optionally be provided
with a name and contains the subsequent result. The text \texttt{by
  simp} means that the proof of this statement only needs
simplifications that Isabelle itself can perform and, as a result,
they no longer have to be written out explicitly. In the text excerpt
itself, using the assumption \texttt{Q} and taking into account the
definition of \texttt n (\texttt{n\_def}), it is proven that \texttt{n
  = 0}. This statement is abbreviated as \texttt{n0} to allow access
to it later. In the very next line, \texttt{n0} is used to show the
statement $\alpha_b(n)$ abbreviated with \texttt{Abn}.

The next line shows the statement \texttt{"?P"}. This refers back to
the lemma \texttt{divisibility\_lemma1}, which Benedikt proved
earlier. In addition, the two assumptions $b>2$ and $k>0$ are taken
into account. Furthermore, \texttt{mult\_eq\_0\_iff} is used. This is
a statement implemented in Isabelle earlier by other mathematicians
and which has proven helpful here.

Because it has been shown that \texttt{?P} results from \texttt{?Q},
it must also be shown that \texttt{?Q} also results from \texttt{?P}
(the other direction of equivalence). Therefore, we use next to
introduce the step of the proof and now assume \texttt{?P} (again
abbreviated as \texttt P) is true. The text \texttt{qed} is at the end
of the proof and closes the proof for Isabelle.

Certain steps in Isabelle proved to be more difficult than expected.
The expressions \texttt{sorry} and \texttt{oops} are available for
those statements which are assumed to be correct but which still must
be proven. This makes it possible to leave a proof incomplete at first
in order to begin continuing work at another point. The significant
difference is that Isabelle can access statements with \texttt{sorry}
during subsequent processing (they are accepted as true without
proof), while this is not the case for \texttt{oops}. The proof for
such statements still has to be added at a later point regardless.

The results of the third chapter are, however, now completely
implemented and formalised--Isabelle has seamlessly reproduced why and
how exponentiating can be expressed in Diophantine terms.

\subsubsection{Second and Fourth chapters}
All of the progress in the area of the second and fourth chapter was
made by Abhik and Marco to date. So far, Marco implemented all of the
definitions and functions from Chapter 4 related to modelling register
machines. Abhik formalised the positional notation with associated
attributes such as the \texttt{digit} function from Chapter 2 and the
conversion between the various representations of the table from
Chapter 4. However, we are in constant communication with the other
group members (see Appendix) and are discussing the best approach for
implementation. In the final phase, we are now working on being able
to include everyone in the implementation despite varying levels of
experience.

The second chapter only provides a means to an end that is used in the
fourth chapter. Therefore, following paragraphs focus on the register
machines from the fourth chapter. Required results from the second
chapter are referenced as needed.

The paper [?] proved to be very helpful for implementing a register
machine. In 2013, Xu, Zhang and Urban formalised a Turing machine in
order to implement the undecidability of the halting problem in
Isabelle. We have transferred the approaches from their Chapter 2 that
they used for implementation for Turing machines to register machines
in simulate register machines.

The various types of instructions are the most fundamental components
of a register machine. We represent this in an instruction. In this
representation, the names \texttt{Add}, \texttt{Sub}, and
\texttt{Halt} refer exactly to the state types $\mathrm R++$,
$\mathrm R--$, and $\mathrm{HALT}$ explained further above.

<figure>

Both the \texttt{Add} and \texttt{Sub} instructions each refer to a
register $\mathrm Rl$, which means the container for the value of the
register in this case. These instruction are thus associated with a
natural number $l$, which identifies the register to be changed.
Taking inspiration from Turing machines, we refer to a list of all
register values as the tape for our register machine. Similarly, the
states associated with instructions (state) are natural numbers that
identify the next instruction $\mathrm Si$. We refer to the list of
all instructions as the program for the register machine.

Ultimately, the numbers $l$ and $i$ are the indices for the tape and
program that we use to access the corresponding register values and
subsequent states. In Isabelle, register and state are synonyms for
the nat data type for natural numbers, which help simplify the
readability of the code for us and the reader.

We formalise one column of the table (called a configuration of the
register machine), i.e. a snapshot of all register values and the
current state, with the configuration data type. The following
excerpt, using the \texttt{type\_synonym} keyword, defines an alias
for a 2-tuple with a current instruction and the current tape.

<figure>

In this way, we provided Isabelle with all the essential definitions
of the individual components for a register machine. The following two
excerpts are used to transition from one configuration at the time $t$
to the next configuration at the time $t + 1$ (the step function). We
begin with simple functions that we then combine into complex
functions and this ultimately lets us write the desired step
functions.

<figure>

The function fetch here provides the next instruction in each case,
based on the instruction of the current state. The signature of this
function \texttt{program => instruction => nat => instruction} shows
that fetch is passed the program, i.e. the list of all instructions
and the current instruction and the value of the register to be
processed, and returns an instruction. This is achieved using the
Indexing operator list!index (usually list[index] in other programming
languages), which reads the corresponding instruction from the
program. If necessary, the passed register value val is compared to
zero in order to decide whether $\mathrm Si$ or $\mathrm Sj$ is
executed next.

Now we want to call the next state and execute the current
instruction, i.e. change a register value. We write the following
function update for this. It takes a tape and the current instruction
as input and returns the modified tape after execution.

<figure>

The update function then tests which type of instruction is to be
executed for a given current state. Using the indexing operator, the
function has access to the current register value \texttt{t!r} and can
either increase it or decrease it by one (if possible). If the
register machine finds itself in the Halt state, the register value
does not change.

The increase or decrease in a register value is achieved using the
Isabelle function \texttt{list\_update l i n}, which replaces the
entry at index $i$ in list $l$ with a new value $n$. In lines 2 and 3,
the underscores mean that it still does not matter which subsequent
state is referenced because only the current instruction and the
current register are of any significance for this function.

By combining the two functions above, we can finally take the step
from one time step to the next and thereby simulate any register
machine.

<figure>

One of the next short-term goals is to show the table, which is
currently mapped column by column (using the configuration), in series
in order to map it in positional notation.

The means for this is already implemented in
\texttt{PositionalNotation.thy} from Chapter 2. We represent two
attributes in the \texttt{pos} data type: First the base $b$ --- a
natural number --- and second a list of natural numbers --- the
numerals. In this set-up, the first element in this list corresponds
to the lowest place value --- the ones' place $b^0$ --- and the last
element to the highest place value. In other words, we use a
\emph{least significant bit} order.

<figure>

In combination with this formalisation of a positional notation, we
have also implemented and proven some attributes and properties such
as masks or orthogonal binary sequences. These will be used once we
reach the next goal mentioned above and formalise the table in series.

One essential function for converting between various ways of
displaying the table and thus, ultimately, for the proof being sought
of representing recursively enumerable sets as Diophantine sets
remains the digit function, which extracts an individual numeral from
a number in base $B$.

<figure>

Chapter 4 is completed once we prove the main equivalence from Section
4.5 from [?] between termination of a register machine and the
existence of a correct table. Using a few sorry functions, we have
also already combined Chapter 3 and 4. We are just missing the
aforementioned proofs for fully finalising the formalisation of
Matiyasevich's proof.

\section{Discussion}
Learning the syntax used by Isabelle was one significant conceptual
difficulty. Since we had only amassed experience in traditional
programming up to this point, we needed some time to get used to
Isabelle.

In gaining this new experience, we benefited from the fact that our
informatics coursework provided an introduction to the functional
language Haskell. This simplified the transition to Isabelle
significantly because Isabelle is also based on functional paradigms.
Another significant challenge was formally coding and implementing a
register machine, which is generally viewed as more of a conceptual
idea.

When we started work on the project in October 2017, we still had not
gotten much of a sense of these difficulties. With the knowledge we
have today, we can say that the scope and complexity of the proof made
it impossible to formalise it in full in the time available. In
actuality, even Isabelle experts like Lawrence Paulson of the
University of Cambridge, who is one of Isabelle's creators, called the
work a ``project for a doctoral thesis.'' [?] Despite all of this, we
succeeded in formalising the latest and most difficult part of the
proof (the missing puzzle piece).

At present, we estimate we have formalised one third of the fourth
chapter. The entire group's progress can be assessed as about 70\%.
Because we now have a good overview of the problem as a whole and the
complexity of the formalisation work, we are able to provide an
overview of the tasks that are still outstanding and to provide a
realistic estimate of when the project should be complete.

\subsection{Subsequent plans}
In Chapter 4, the next step has to be to prove the correctness of the
equations that the register machines are simulating. Referring back to
the previously implemented relationships from Matiyasevich's second
chapter, the fourth chapter is closely tied to the result of the
third. This is expected to be in place by the start of the competition
in September in any case.

Once the formalisation is complete, we will provide our formalisation
to the Isabelle community. This will give everyone who uses Isabelle
in the future the opportunity to access our proof. This means that,
from that point on, everyone using recursively equivalent sets in
their theory can utilise the equivalence to Diophantine sets.

\section{Conclusion}
Interactive proof assistants have become more intelligent and more
powerful in the last decade. As a result of this, more and more
complex mathematical results were and are able to be formally
verified. Taking a look back at the list of Hilbert's 23 problems,
however, makes it clear that, while 14 have been solved in full and 6
in part, not a single proof has been formalised for these problems.
The successful finalisation of our project will represent the first
formal verification of a Hilbert problem.

\section{Bibliography}

\section{Acknowledgments}

\appendix

\end{document}
