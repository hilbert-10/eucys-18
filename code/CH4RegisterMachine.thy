theory CH4RegisterMachine
imports Main List
begin

(* Joint Work of Abhik Pal, Marco David, Scott Deng *)

subsection {* Basic Datatype Definitions *}

(* Type synonyms for registers (= register indices) the "tape" (sim. to a
 * Turing machine) that contains a list of register values.
 *)
type_synonym register = nat
type_synonym tape = "register list"

(* The register machine understands "instructions" that operate on state(-id)s
 * and modify register(-id)s. The machine stops at the HALT instruction.
 *)
type_synonym state = nat
datatype instruction =
  isadd: Add (modifies : register) (goes_to : state) |
  issub: Sub (modifies : register) (goes_to : state) (goes_to_alt : state) |
  ishalt: Halt
where
  "modifies Halt = 0" |
  "goes_to_alt (Add _ next) = next"

(* A program, then, just becomes a list of these instructions *)
type_synonym program = "instruction list"

(* A configuration of the (runtime of) a machine encodes information about the
 * instruction and the state of the registers (i.e., the tape). We describe it
 * here as a tuple.
 *)
type_synonym configuration = "(state * tape)"

subsection {* Essential Functions to operate the Register Machine *}

(* Given a tape of register values and some instruction(s) the register
 * machine first reads the value of the register from the tape (by convention
 * assume that the value "read" by the HALT state is zero). The machine then,
 * fetches the next instruction from the program, and finally updates the
 * tape to reflect changes by the last instruction.
 *)
fun read :: "tape \<Rightarrow> instruction \<Rightarrow> nat"
  where
    "(read t (Add r _)) = t!r"
  | "(read t (Sub r _ _)) = t!r"
  | "(read t Halt) = 0"

definition read_new :: "tape \<Rightarrow> program \<Rightarrow> state \<Rightarrow> nat"
  where "read_new t p s = t ! (modifies (p!s))"

fun fetch_old :: "program \<Rightarrow> state \<Rightarrow> instruction \<Rightarrow> nat \<Rightarrow> state"
  where
    "(fetch_old p s (Add r next) _) = next"
  | "(fetch_old p s (Sub r next nextalt) val) = (if val = 0 then nextalt else next)"
  | "(fetch_old p s Halt _) = s"

definition fetch :: "state \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> state" where
  "fetch s p v = (if issub (p!s) \<and> v = 0 then goes_to_alt (p!s)
                       else if ishalt (p!s) then s
                       else goes_to (p!s))"                                              

lemma fetch_equiv:
  assumes "i = p!s"
  shows "fetch s p v = fetch_old p s i v"
  by (cases i; auto simp: assms fetch_def)
 
fun update_old :: "tape \<Rightarrow> instruction \<Rightarrow> tape"
  where
    "(update_old t (Add r _)) = (list_update t r (t!r + 1))"
  | "(update_old t (Sub r _ _)) = (list_update t r (if (t!r = 0)
                                                then 0 else (t!r) - 1))"
  | "(update_old t Halt) = t"

definition update :: "tape \<Rightarrow> instruction \<Rightarrow> tape" where
  "update t i = (if ishalt i then t
                    else if isadd i then list_update t (modifies i) (t!(modifies i) + 1)
                    else list_update t (modifies i) (if t!(modifies i) = 0 then 0 else (t!(modifies i)) - 1) )"

lemma update_equiv: "update t i = update_old t i"
  by (cases i; auto simp: update_def)

(* Finally, to "run" the machine we describe a function that --- given the
 * current state of the machine as a configuration and the program --- lets us
 * "step" to the next configuration of the machine.
 *
 * We then use this to get a function that does multiple steps of the machine
 * (while keeping track of its execution history).
 *)
definition step :: "configuration \<Rightarrow> program \<Rightarrow> configuration"
  where
    "(step ic p) = (let nexts = fetch (fst ic) p (read (snd ic) (p!(fst ic)));
                        nextt = update (snd ic) (p!(fst ic))
                        in (nexts, nextt))"
(* p!s = program index state = instruction *)

fun steps :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> configuration"
  where
    "(steps c p 0) = c"
  | "(steps c p (Suc n)) = (step (steps c p n) p)"

lemma step_commutative: "steps (step c p) p t = step (steps c p t) p"
  by (induction t; auto)

lemma step_fetch_correct:
  fixes t :: nat
    and c :: configuration
    and p :: program
  assumes "is_valid c p"
  defines "ct \<equiv> (steps c p t)"
  shows "fst (steps (step c p) p t) = fetch (fst ct) p (read (snd ct) (p ! (fst ct)))"
  using ct_def step_commutative step_def by auto

(* fst (step (steps (0, tape) p t) p) *)

subsection {* Auxiliary Functions for Proofs *}

(* Zero Function for tape and register *)
fun zero :: "tape \<Rightarrow> register \<Rightarrow> nat"
  where
    "zero t l = (if 0 = t!l then 0 else 1)"

subsection {* From Configurations to a Protocol *}
text {* The functions R, S, Z are used with the following arguments:

 1)  c    - initial configuration (state0, tape)
 2)  p    - program
 3)  n/k  - row (state or register index)
 4)  t    - column (time)

*}

subsubsection {* Register Values *}

definition R :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat"
  where "R c p n t = (snd (steps c p t)) ! n"

fun RL :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat"  where
  "RL c p b 0 l = ((snd c) ! l)" |
  "RL c p b (Suc t) l = ((snd c) ! l) + b * (RL (step c p) p b t l)"

declare RL.simps[simp del]
lemma RL_simp: 
  "RL c p b (Suc t) l = (snd (steps c p (Suc t)) ! l) * b ^ (Suc t) + (RL c p b t l)"
proof (induction t)
  case 0
  thus ?case by (auto simp: RL.simps)
next
  case (Suc t)
  then show ?case apply (auto simp: Suc) sorry
qed

subsubsection {* State Values *}

definition S :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat"
  where "S c p k t = (if (fst (steps c p t) = k) then 1 else 0)"

definition S2 :: "configuration \<Rightarrow> nat \<Rightarrow> nat"
  where "S2 c k = (if (fst c) = k then 1 else 0)"

fun SK :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat"
(*   where "SK c p b q k = (SUM t=0..q. (S c p k t) * b ^ t)"
 *)
  where "SK c p b 0 k = (S2 c k)" |
   "SK c p b (Suc t) k = (S2 c k) + b * (SK (step c p) p b t k)"

declare SK.simps[simp del]
lemma SK_simp: 
  "SK c p b (Suc t) k = (S2 (steps c p (Suc t)) k) * b ^ (Suc t) + (SK c p b t k)"
proof (induction t)
  case 0
  thus ?case by (auto simp: SK.simps)
next
  case (Suc t)
  then show ?case apply (auto simp: Suc) sorry
qed
(*
lemma [simp]:
  \<open>SK c p b 0 k = S c p k 0\<close>
  \<open>SK c p b (Suc q) k = (S c p k (Suc q)) * b ^ (Suc q) + SK c p b q k\<close>
  unfolding SK_def by auto
*)

subsubsection {* Zero-Indicator Values *}

definition Z :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat"  where
   "Z c p n t = (if (R c p n t > 0) then 1 else 0)"

definition Z2 :: "configuration \<Rightarrow> nat \<Rightarrow> nat" where
   "Z2 c n = (if (snd c) ! n > 0 then 1 else 0)"

fun ZL :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat"
  where "ZL c p b 0 l = (Z2 c l)" |
   "ZL c p b (Suc t) l = (Z2 c l) +  b * (ZL (step c p) p b t l)"

declare ZL.simps[simp del]
lemma ZL_simp: 
  "ZL c p b (Suc t) l = (Z2 (steps c p (Suc t)) l) * b ^ (Suc t) + (ZL c p b t l)"
proof (induction t)
  case 0
  thus ?case by (auto simp: ZL.simps)
next
  case (Suc t)
  then show ?case apply (auto simp: Suc) sorry
qed

subsubsection {* Run Function *}

(* given an initial configuration, execute `step` until in halt state
 * return list of all configurations (length = q) *)
(* impossible to implement, because termination order requires termination of the RM *)
(* is given maximal length via parameter q :: nat -- implicit assumption of termination *)
fun run :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> configuration list"
  where
    "run c _ 0 = [c]" |
    "run (s, t) p (Suc q) = (if (p!s = Halt) then [(s, t)] else (s, t) # (run (step (s, t) p) p) q)"

subsection {* Protocol Properties *}

lemma Z_bounded[simp]:
  fixes p :: program
  fixes c :: configuration
  fixes n t :: nat
  shows "Z c p n t \<le> 1"
  by (auto simp: Z_def)

lemma S_bounded[simp]:
  fixes p :: program
  fixes c :: configuration
  fixes k t :: nat
  shows "S c p k t \<le> 1"
  using S_def by auto

subsection {* Validity Checks and Assumptions *}
(* Jointly Implemented by Scott Deng and Marco David *)

subsubsection {* Helper Functions *}

(* check bound for each type of instruction *)
(* take a m representing the upper bound for state number *)
fun instruction_state_check :: "nat \<Rightarrow> instruction \<Rightarrow> bool"
  where "instruction_state_check _ Halt = True"
  |     "instruction_state_check m (Add _ ns) = (ns < m)"
  |     "instruction_state_check m (Sub _ ns1 ns2) = ((ns1 < m) & (ns2 < m))"

fun instruction_register_check :: "nat \<Rightarrow> instruction \<Rightarrow> bool"
  where "instruction_register_check _ Halt = True"
  |     "instruction_register_check m (Add n _) = (n < m)"
  |     "instruction_register_check m (Sub n _ _) = (n < m)"

(* passes function via currying into list_all *)
fun program_state_check :: "program \<Rightarrow> bool"
  where "program_state_check p = list_all (instruction_state_check (length p)) p"

fun program_register_check :: "program \<Rightarrow> nat \<Rightarrow> bool"
  where "program_register_check p n = list_all (instruction_register_check n) p"

fun tape_check :: "tape \<Rightarrow> nat \<Rightarrow> bool"
  where "tape_check t a = (t \<noteq> [] & hd t = a & list_all (\<lambda>x. x = 0) (tl t))"

fun program_includes_halt :: "program \<Rightarrow> bool"
  where "program_includes_halt p = (p \<noteq> [] \<and> last p = Halt \<and> Halt \<notin> set (butlast p))"

subsubsection {* Is Valid & Terminates *}

(* Final Validity Check / Assumption of Initial Configuration & Program *)
fun is_valid (*:: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> bool"*)
  where "is_valid (s, t) p = 
           ((program_state_check p) 
         &  (program_register_check p (length t))
         &  (program_includes_halt p))"

fun is_valid_initial
  where "is_valid_initial (s, t) p a = ((is_valid (s, t) p)
                                     \<and> (tape_check t a)
                                     \<and> (s = 0))"

fun terminates :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> bool"
  where "terminates c p q = (is_valid c p 
                          \<and> p ! fst (steps c p q) = last p
                          \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps c p x))))"

(* takes c :: nat, the exponent defining the base b *)
fun cells_bounded :: "configuration \<Rightarrow> program \<Rightarrow> nat \<Rightarrow> bool" where
  "cells_bounded conf p c = ((\<forall>l t. 2^c > R conf p l t)
                          \<and>  (\<forall>k t. 2^c > S conf p k t)
                          \<and>  (\<forall>l t. 2^c > Z conf p l t))"

(* PLAYING AROUND: RECURSIVELY ENUMERABLE PREDICATE *)
definition is_recenum :: "nat set \<Rightarrow> bool" where
  "is_recenum A = (\<exists> p :: program. \<forall> a :: nat. \<exists> q n :: nat.
    (a \<in> A) \<longrightarrow> (Halt = p ! (fst (steps (0, a # replicate 0 n) p q))))"

(* value "is_recenum (set [nat 1, 2, 3])" *)

end