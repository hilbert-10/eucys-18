(*
 * 2.6 
 * DIGIT BY DIGIT COMPARISON OF NATURAL NUMBERS
 * Helper Methods
 *)

theory CH2DigitComp
imports Main CH2PositionalNotation
(* "HOL-Word.Bits_Bit"
"HOL-Word.Bool_List_Representation" *)
begin

(* Testing with the Isabelle HOL Bits Library
instantiation nat :: bits
begin

definition test_bit_nat :: \<open>nat \<Rightarrow> nat \<Rightarrow> bool\<close> where
  "test_bit i j = test_bit (int i) j"

definition lsb_nat :: \<open>nat \<Rightarrow> bool\<close> where
  "lsb i = (int i :: int) !! 0"

definition set_bit_nat :: "nat \<Rightarrow> nat \<Rightarrow> bool \<Rightarrow> nat" where
  "set_bit i n b = nat (bin_sc n b (int i))"

definition set_bits_nat :: "(nat \<Rightarrow> bool) \<Rightarrow> nat" where
  "set_bits f =
  (if \<exists>n. \<forall>n'\<ge>n. \<not> f n' then
     let n = LEAST n. \<forall>n'\<ge>n. \<not> f n'
     in nat (bl_to_bin (rev (map f [0..<n])))
   else if \<exists>n. \<forall>n'\<ge>n. f n' then
     let n = LEAST n. \<forall>n'\<ge>n. f n'
     in nat (sbintrunc n (bl_to_bin (True # rev (map f [0..<n]))))
   else 0 :: nat)"

definition shiftl_nat where
  "shiftl x n = nat ((int x) * 2 ^ n)"

definition shiftr_nat where
  "shiftr x n = nat (int x div 2 ^ n)"

definition bitNOT_nat :: "nat \<Rightarrow> nat" where
  "bitNOT i = nat (bitNOT (int i))"

definition bitAND_nat :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "bitAND i j = nat (bitAND (int i) (int j))"

definition bitOR_nat :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "bitOR i j = nat (bitOR (int i) (int j))"

definition bitXOR_nat :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "bitXOR i j = nat (bitXOR (int i) (int j))"

instance ..

end

*)

(* element wise product of two lists. *)
fun hl_list_prod :: "nat list => nat list => nat list"
  where
    "(hl_list_prod [] []) = [0]"
  | "(hl_list_prod as []) = [0]"
  | "(hl_list_prod [] bs) = [0]"
  | "(hl_list_prod (a # as) (b # bs)) = (a * b) # (hl_list_prod as bs)"

lemma [simp]: "(hl_list_prod as []) = [0]"
  using hl_list_prod.elims by blast

(* mimics binary bitwise and *)
fun digit_binary_mult :: "nat => nat => nat" (infixl "&&" 70)
  where
    "(digit_binary_mult a b) = (pnval (Pos 2 (hl_list_prod
                                              (hl_digit_list a 2)
                                              (hl_digit_list b 2))))"

(* Trying to prove equivalence of our fct with Isabelle HOL Library function 
lemma "digit_binary_mult a b = a AND b"
  unfolding bitAND_nat_def
  apply (induction a arbitrary: b)
  subgoal for b
    by (cases "hl_digit_list b 2") auto
  subgoal for a b
    apply (auto simp: nat_add_distrib numeral_2_eq_2)
    oops
*)

fun orthogonal :: "nat => nat => bool"
  where
    "(orthogonal a b) = (if ((digit_binary_mult a b) = 0) then True else False)"

(* checks if a >= b for a in as and b in bs *)
fun hl_list_comp :: "nat list => nat list => bool list"
  where
    "(hl_list_comp [] []) = [True]"
  | "(hl_list_comp as []) = [True]"
  | "(hl_list_comp [] bs) = [False]"
  | "(hl_list_comp (a # as) (b # bs)) = (a >= b) # (hl_list_comp as bs)"

lemma [simp]: \<open>(hl_list_comp as []) = [True]\<close>  by (cases as) auto

(* a "masks" b iff a >= b digit wise in binary *)
fun masks :: "nat => nat => bool" (infixl "msk" 70)
  where
    "(masks a b) = (fold (%x y. x & y)
                         (hl_list_comp (hl_digit_list a 2) (hl_digit_list b 2))
                         True)"

end