theory CH2Diophantine
imports
  Main Binomial "~~/src/HOL/Library/Discrete"
  CH2PositionalNotation CH2DigitComp
begin

section \<open>Number-theoretical prerequisites\<close>

(* 2.4 *) 
subsection \<open>Positional Notation\<close>

(* Proven by Marco David *)
lemma lm02_29_digit_func:
  fixes a b k :: nat
  assumes "b > 1"
  shows "(d = digit a b k) = (EX x y. ((a = x * b^(Suc k) + (d*b^k) + y)
                                      & (d < b)
                                      & (y < b^k)))"
          (is "?P = ?Q")
proof 
  assume "?P" (*get x, y from base b representation of a*)
  define x where x: "x \<equiv> a div b^(Suc k)"
  define y where y: "y \<equiv> a mod b^k"
  from x y assms have 1: "a = x * b^(Suc k) + (d * b^k) + y"
    by (metis Divides.mod_mult2_eq Groups.mult_ac(2) \<open>d = digit a b k\<close> digit.simps mult_div_mod_eq power_Suc2 semiring_div_class.mod_mult_self4 semiring_normalization_rules(25))
  from y have 3: "y < b^k"
    using assms by auto
  from assms have "a mod b^(Suc k) < b^(Suc k)"
    by (simp add: assms)
  from this have "(a mod b^(Suc k)) div b^k < b^(Suc k) div b^k"
    by (metis Divides.div_mult2_eq div_eq_0_iff div_mult_self1_is_m gr_implies_not0 neq0_conv 
        power_eq_0_iff power_not_zero semiring_normalization_rules(28))
  from this have 2: "d < b"
    using \<open>d = digit a b k\<close> assms by auto
  thus "?Q"
    using "1" "3" by blast
next
  assume q: "?Q" (*get a in base b from x, y in base b *)
  from this obtain x y where xy: "(a = x * b^(Suc k) + (d*b^k) + y) \<and> (y < b^k) \<and> (d < b)" by blast
  from this assms have "((x * b^(Suc k) + d * b^k) mod (b^(Suc k))) = (d * b^k)" by auto
  from xy have 1: "y < b^k" by auto
  from xy have 2: "d < b" by auto
  from 1 2 have 3: "d * b^k + y < (d + 1) * b^k" by simp
  from xy have "(d + 1) * b^k \<le> b * b^k"
    by (metis One_nat_def Suc_leI add.right_neutral add_Suc_right mult_le_mono1)
  with 1 2 have "d * b^k + y < b^(Suc k)"  by auto
  from this assms have "(d * b^k + y) mod b^(Suc k) = (d * b^k + y)" by auto
  then have "(a mod b^(Suc k)) = (d * b^k + y)"
    by (simp add: add.commute semiring_normalization_rules(25) xy)
  from this xy have "(a mod b^(Suc k)) div b^k = d" by auto
  thus "?P" by auto
qed

(* 2.5 *)
subsection \<open>Binomial coefficients\<close>

(* work-in-progress by Maria Oprea *)
lemma lm02_34_bin_coeff_digit:
  fixes a b c :: nat
  assumes "a > b"
  shows "(c = (a choose b)) = (EX u. (u = 2^a + 1) 
                                      & (c = (digit ((u + 1)^a) u b)))"
          (is "?P = ?Q")
  sorry

(* KUMMER'S / LUKAS' THEOREM (2.5.2) work-in-progress by Deepak Aryal *)

(* 2.6, work-in-progress by Jonas Bayer *)
subsection \<open>Digit-by-digit comparison of natural numbers\<close>

lemma lm02_41_ortho_odd_binomial:
  fixes a b :: nat
  shows "(orthogonal a b) = (odd ((a + b) choose b))" (is "?P = ?Q")
  sorry

lemma lm02_43_masking:
  fixes b c :: nat
  shows "(masks c b) = (odd (c choose b))" (is "?P = ?Q")
  sorry

lemma lm02_47_digit_mult:
  fixes a b c :: nat
  shows "(c = (digit_binary_mult a b)) = ((masks a c)
                                          & (masks b c)
                                          & (orthogonal (a - c) (b - c)))"
        (is "?P = ?Q")
  sorry


section {* Diophantine Representation *}
(* ============================================================================ *)
(* Work of Yufei Liu, all Lemmas are fully proven, proofs are not yet published *)

(* definition of Diophantine polynomials *)
datatype diophantine = const nat | var nat | sum diophantine diophantine (infixl "[+]" 80) | diff diophantine diophantine (infixl "[-]" 80) | prod diophantine diophantine (infixl "[*]" 81)

(* value of a polynomial given a list of variable values *)
fun val:: "(nat \<Rightarrow> nat) \<Rightarrow> diophantine \<Rightarrow> int"
  where
  "val f (const c) = int c"
  | "val f (var x) = f x"
  | "val f (p [+] q) = (val f p) + (val f q)"
  | "val f (p [-] q) = (val f p) - (val f q)"
  | "val f (p [*] q) = (val f p) * (val f q)"

(* The set of indices of variables *)

fun var_space :: "diophantine \<Rightarrow> nat set"
  where
"var_space (const c) = {}"
| "var_space (var x) = {x}"
| "var_space (d1 [+] d2) = (var_space d1) \<union> (var_space d2)"
| "var_space (d1 [-] d2) = (var_space d1) \<union> (var_space d2)"
| "var_space (d1 [*] d2) = (var_space d1) \<union> (var_space d2)"

lemma var_space_finite:
  fixes D
  shows "finite (var_space D)"
  by (induct D) auto

(* Substitution of variables *)

fun var_subst :: "diophantine  \<Rightarrow> (nat \<Rightarrow> nat) \<Rightarrow> diophantine"
  where
"var_subst (const c) _ = const c"
| "var_subst (var x) f = var (f x)"
| "var_subst (d1 [+] d2) f = (var_subst d1 f) [+] (var_subst d2 f)"
| "var_subst (d1 [-] d2) f = (var_subst d1 f) [-] (var_subst d2 f)"
| "var_subst (d1 [*] d2) f = (var_subst d1 f) [*] (var_subst d2 f)"

(* Diophantine representation and set *)

definition representation :: "nat set \<Rightarrow> (nat \<Rightarrow> nat) set \<Rightarrow> diophantine \<Rightarrow> bool"
  where
"representation mask S D = (\<forall>f. f \<in> S \<longleftrightarrow> (\<exists>g. (\<forall>i \<in> mask. f i = g i) \<and> val g D = 0))"

definition is_diophantine:: "nat set \<Rightarrow> (nat \<Rightarrow> nat) set \<Rightarrow> bool"
  where
"is_diophantine mask S = (\<exists>D. representation mask S D)"

(* Left invertibility *)

definition left_inverse::"(nat \<Rightarrow> nat) \<Rightarrow> (nat \<Rightarrow> nat) \<Rightarrow> bool"
  where
"left_inverse f g \<longleftrightarrow> (\<forall>x. g (f x) = x)"

definition is_left_invertible::"(nat \<Rightarrow> nat) \<Rightarrow> bool"
  where
"is_left_invertible f = (\<exists>g. left_inverse f g)"

(* Definition of a fixed point (set) of a function *)
definition is_fixed_point :: "(nat \<Rightarrow> nat) \<Rightarrow> nat set \<Rightarrow> bool"
  where
"is_fixed_point f S = (\<forall>x \<in> S. f x = x)"

(* Some useless definitions and lemmas *)

definition is_injective:: "(nat \<Rightarrow> nat) \<Rightarrow> bool"
  where
"is_injective f = (\<forall>x y. f x = f y \<longrightarrow> x = y)"

definition is_surjective:: "(nat \<Rightarrow> nat) \<Rightarrow> bool"
  where
"is_surjective f = (\<forall>y. \<exists>x. f x = y)"

definition inverse:: "(nat \<Rightarrow> nat) \<Rightarrow> (nat \<Rightarrow> nat) \<Rightarrow> bool"
  where
"inverse f g \<longleftrightarrow> (\<forall>x. f (g x) = x \<and> g (f x) = x)"

definition is_invertible:: "(nat \<Rightarrow> nat) \<Rightarrow> bool"
  where
"is_invertible f = (\<exists>g. inverse f g)"

lemma left_inverse_lemma:
  fixes f
  shows "is_left_invertible f \<Longrightarrow> is_injective f"
proof -
  show ?thesis sorry
qed

lemma inverse_lemma:
  fixes f
  shows "is_invertible f \<Longrightarrow> is_injective f \<and> is_surjective f"
proof -
  show ?thesis sorry
qed

lemma var_subst_lemma:
  fixes val_f and subst and D
  assumes "is_left_invertible subst"
  shows "\<exists>subst_inv. left_inverse subst subst_inv \<and> val val_f D = val (\<lambda>x. val_f (subst_inv x)) (var_subst D subst)"
proof -
  show ?thesis sorry
qed

lemma var_space_subst_lemma:
  fixes D and subst
  shows "var_space (var_subst D subst) = {y. (\<exists>x \<in> var_space D. y = subst x)}"
  by (induct D) auto

lemma var_shift_lemma:
  fixes mask and m
  assumes "\<forall>n\<in>(var_space D1 \<union> mask). m > n"
  defines "subst \<equiv> (\<lambda>x. if x \<in> mask then x else (x + m))"
  defines "subst_inv \<equiv> (\<lambda>x. if x \<in> mask then x else (x - m))"
  shows "left_inverse subst subst_inv"
  using assms subst_def left_inverse_def by auto

lemma subst_ex_lemma:
  fixes D1 D2 and mask
  assumes "finite mask"
  shows "\<exists>subst. is_left_invertible subst \<and> is_fixed_point subst mask \<and> var_space D1 \<inter> var_space (var_subst D2 subst) \<subseteq> mask"
proof -
  show ?thesis sorry
qed

lemma val_lemma:
  fixes f g::"nat \<Rightarrow> nat" and D::diophantine
  shows "(\<forall>x \<in> var_space D. (f x = g x)) \<longrightarrow> val f D = val g D"
  by (induct D) auto

lemma conj_lemma:
  fixes f mask D1 D2
  assumes "var_space D1 \<inter> var_space D2 \<subseteq> mask"
  shows "(\<exists>g1 g2. (\<forall>i \<in> mask. f i = g1 i \<and> f i = g2 i) \<and> val g1 D1 = 0 \<and> val g2 D2 = 0) \<Longrightarrow> (\<exists>g. (\<forall>i \<in> mask. f i = g i) \<and> (val g ((D1 [*] D1) [+] (D2 [*] D2))) = 0)"
proof -
  show ?thesis sorry
qed

lemma disj_lemma:
  fixes f mask D1 D2
  assumes "var_space D1 \<inter> var_space D2 \<subseteq> mask"
  shows "(\<exists>g. (\<forall>i \<in> mask. f i = g i) \<and> val g (D1 [*] D2) = 0) \<Longrightarrow> (\<exists>g1 g2. (\<forall>i \<in> mask. f i = g1 i) \<and> (val g1 D1 = 0) \<or> (\<forall>i \<in> mask. f i = g2 i) \<and> (val g2 D2 = 0))"
  by auto

lemma representation_conjunction:
  fixes S1 S2::"(nat \<Rightarrow> nat) set" and D1 D2::diophantine and mask::"nat set"
  assumes "representation mask S1 D1 \<and> representation mask S2 D2" and "var_space D1 \<inter> var_space D2 \<subseteq> mask"
  shows "representation mask (S1 \<inter> S2) ((D1 [*] D1) [+] (D2 [*] D2))" 
proof -
  show ?thesis sorry
qed

lemma subst_diophantine:
  fixes f and mask and subst
  assumes "is_left_invertible subst \<and> is_fixed_point subst mask"
  shows "(\<exists>g. (\<forall>i \<in> mask. f i = g i) \<and> val g D = 0) \<longleftrightarrow> (\<exists>g. (\<forall>i \<in> mask. f i = g i) \<and> val g (var_subst D subst) = 0)"
proof -
  show ?thesis sorry
qed

lemma representation_disjunction:
  fixes S1 S2::"(nat \<Rightarrow> nat) set" and D1 D2::diophantine and mask::"nat set"
  assumes "representation mask S1 D1 \<and> representation mask S2 D2" and "var_space D1 \<inter> var_space D2 \<subseteq> mask"
  shows "representation mask (S1 \<union> S2) (D1 [*] D2)" 
proof -
  show ?thesis sorry
qed

lemma diophantine_union_inter:
  fixes S1 S2::"(nat \<Rightarrow> nat) set" and mask
  assumes "finite mask" and "is_diophantine mask S1 \<and> is_diophantine mask S2"
  shows "is_diophantine mask (S1 \<inter> S2) \<and> is_diophantine mask (S1 \<union> S2)"
proof -
  show ?thesis sorry
qed

lemma conjunction:
  fixes A B::int
  shows "A = 0 \<and> B = 0 \<longleftrightarrow> A ^ 2 + B ^ 2 = 0" by auto

lemma disjunction:
  fixes A B::int
  shows "A = 0 \<or> B = 0 \<longleftrightarrow> A * B = 0" by auto

(* 2.22 *)
lemma less_is_diophantine:
  fixes a b::nat
  shows "a < b \<longleftrightarrow> (\<exists>x. int a - int b + int x + 1 = 0)"
proof -
  show ?thesis sorry
qed

lemma less_lemma:
  fixes s
  defines "mask \<equiv> {0, 1, 2}" and "d \<equiv> (var 0 [-] var 1) [+] var 4 [+] const 1"
  shows "(\<exists>x. int (s 0) - int (s 1) + int x + 1 = 0) \<longleftrightarrow> (\<exists>g. (\<forall>i \<in> mask. s i = g i) \<and> val g d = 0)"
proof -
  show ?thesis sorry
qed

lemma less_diophantine:
  fixes S
  defines "mask \<equiv> {0, 1, 2}"
  assumes "\<forall>s. s \<in> S \<longleftrightarrow> (s 0) < (s 1)"
  shows "is_diophantine mask S"
proof -
  show ?thesis sorry
qed

(* 2.23 *)
lemma dvd_is_diophantine:
  fixes a b::nat
  shows "a dvd b \<longleftrightarrow> (\<exists>x. int a * int x - int b = 0)"
proof -
  show ?thesis sorry
qed

(* If a square root of a natural number is rational, then it is a natural number. *)
lemma nat_square_root:
  fixes i::nat
  assumes "sqrt i \<in> \<rat>"
  shows "sqrt i \<in> \<nat>" 
proof -
  show ?thesis sorry
qed

(* 2.24 *)
lemma mod_is_diophantine:
  fixes a b c::nat
  shows "(c > 0 \<and> a mod c = b mod c \<or> c = 0 \<and> a = b) \<longleftrightarrow> (\<exists>x. (int a - int b) * (int a - int b) - int c * int c * int x = 0)"
proof -
  show ?thesis sorry
qed

(* The actual lemma that we will use. This eliminates the case where the modulus is zero. *)
lemma mod_pos_is_diophantine:
  fixes a b c::nat
  shows "(c > 0 \<and> a mod c = b mod c) \<longleftrightarrow> (\<exists>x1 x2. ((int a - int b) * (int a - int b) - int c * int c * int x1) ^ 2 + (- int c + int x2 + 1) ^ 2 = 0)"
proof -
  show ?thesis sorry
qed

end
