theory CH4Simulation
  imports
    Main
    CH4RegisterMachine
    CH4PositionalEncoding
begin
 
section {* Simulation of Register Machine Transitions by Equations *}
(* Lemmas stated by Marco David *)

(* [T] 4.6 *)
lemma lm04_06_one_step_relation_register:
  fixes l::register
    and ic::configuration (* any configuration at time t, not necessarily initial configuration *)
    and p::program

  defines "s \<equiv> fst ic"
      and "tape \<equiv> snd ic"

  defines "m \<equiv> length p"
      and "tape' \<equiv> snd (step ic p)"

  assumes is_val: "is_valid ic p"
      and l: \<open>l < length tape\<close>

  shows "(tape'!l) = (tape!l) + (if isadd (p!s) \<and> l = modifies (p!s) then 1 else 0)
                              - (zero tape l) * (if issub (p!s) \<and> l = modifies (p!s) then 1 else 0)"
proof -
  show ?thesis
    using l is_val unfolding assms(1-4)
    apply (cases \<open>p!s\<close>; cases ic)
    apply (auto simp: nth_list_update assms(1-4) step_def update_def)
    done
qed

lemma lm04_07_one_step_relation_state:
  fixes d::state
    and c::configuration
    and t::nat
    and p::program

  defines "s \<equiv> S c p"
      and "z \<equiv> Z c p"

  assumes is_val: "is_valid c p"
      and d: "d < length p"

  shows "s d (Suc t)  =    (if isadd (p!k) \<and> d = goes_to (p!k) then s k t else 0)
                        +  (if issub (p!k) \<and> d = goes_to (p!k) then s k t * z (modifies (p!k)) t else 0)
                        +  (if issub (p!k) \<and> d = goes_to_alt (p!k) then s k t * (1 - z (modifies (p!k)) t) else 0)"
proof -
  show ?thesis
    using d is_val unfolding assms s_def S_def z_def Z_def
    apply (cases "p!(fst c)"; cases c)
    defer
    subgoal for a b c s2 t2
      apply(cases "p ! k")
    apply (simp add: step_def update_def fetch_def)

      oops

subsection {* Multiple-step Relations *}

(* [T] 4.22 for registers *)
lemma lm04_22_multiple_register:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q t :: nat
    and a :: nat

  defines "b == B c"
      and "m == length p"
      and "n == length (snd ic)"

assumes is_val: "is_valid_initial ic p a"
assumes c_gt_cells: "cells_bounded ic p c"
assumes l: "l < n"
and "0 < l"

(* assumes terminate: "terminates c p q"
 *)

  defines "r == RL ic p b t"
      and "z == ZL ic p b t"
      and "s == SK ic p b t"

    shows "b^(Suc t) * (R ic p l (Suc t)) + r l = b * r l + b * (\<Sum>k<m. (if isadd (p!k) \<and> l = modifies (p!k) then s k else 0))
                         - b * (\<Sum>k<m. (if issub (p!k) \<and> l = modifies (p!k) then z l && s k else 0))"
proof -
  obtain tape where ic: "ic = (0, tape)" using is_val is_valid_initial.elims(2) by blast
  have sk: "k \<noteq> 0 \<longrightarrow> time = 0 \<longrightarrow> SK ic p (B c) time k = 0" for k time using l c_gt_cells is_val
    by (cases ic; auto simp: S2_def SK.simps)
  show ?thesis
    using l c_gt_cells is_val unfolding assms
  proof (induction t)
    case 0
    have t0: "tape ! l = 0" using 0 
      apply (auto simp add: ic R_def S2_def step_def update_def)
      apply (smt Nitpick.size_list_simp(2) Suc_less_eq Suc_pred assms(7) hd_Cons_tl list_all_length nth_Cons_Suc)
      done
    show ?case
    proof (cases "p!0")
      case (Add x11 x12)
      have "(\<Sum>k<length p. if isadd (p ! k) \<and> l = modifies (p ! k) then SK ic p (B c) 0 k else 0) 
          = (\<Sum>k<length p. if k = 0 then (if l = modifies (p ! k) then SK ic p (B c) 0 k else 0) else 0)"
        apply (rule sum.cong)
        subgoal by simp
        subgoal for x
          apply (cases x)
          defer
          using sk 0 ic apply (simp add: S2_def Add)
          using sk 0 ic apply (simp add: S2_def Add)
          done done

      then have "(\<Sum>k<length p. if isadd (p ! k) \<and> l = modifies (p ! k) then SK ic p (B c) 0 k else 0) 
         = (if l = modifies (p ! 0) then SK (0, tape) p (B c) 0 0 else 0)"
        apply (subst (asm) sum.delta)
        using S2_def 0 ic by (auto)

      moreover have "(\<Sum>k<length p. if issub (p ! k) \<and> l = modifies (p ! k)
        then ZL ic p (B c) 0 l && SK ic p (B c) 0 k else 0) 
         = (\<Sum>k<length p. 0)"
        apply (rule sum.cong)
        subgoal by simp
        subgoal for x
          apply (cases x)
          using sk 0 ic apply (simp_all add: S2_def Add) done done
      ultimately show ?thesis using Add 0 t0
         by (auto simp add: ic R_def S2_def step_def update_def RL.simps SK.simps ZL.simps)
      next
      case (Sub x21 x22 x23)
      then show ?thesis sorry
    next
      case Halt
      then show ?thesis sorry
    qed
  next
    case (Suc t)
    define ct where "ct \<equiv> (steps ic p t)"
    define K where "K \<equiv> fetch (fst ct) p (read (snd ct) (p ! (fst ct)))"
    have K_def_alt: "K = fst (step (steps (0, tape) p t) p)"
      using K_def step_fetch_correct step_def ct_def ic by (auto)
    show ?case
    proof (cases "(p ! K)")
      case (Add x11 x12)
      have "(\<Sum>k<length p. if isadd (p ! k) \<and> l = modifies (p ! k) then S2 (steps (0, tape) p (Suc t)) k * (B c * B c ^ t) + SK (0, tape) p (B c) t k else 0) 
          = (\<Sum>k<length p. (if k = K then (if l = modifies (p ! K) then S2 (steps (0, tape) p (Suc t)) K * (B c * B c ^ t) else 0) else 0) 
                         + (if isadd (p ! k) \<and> l = modifies (p ! k) then SK (0, tape) p (B c) t k else 0))"
        apply (rule sum.cong)
        subgoal by simp
        subgoal for k
        proof (cases "k = K")
          case True
          then show ?thesis
            by (auto simp add: S2_def SK_simp Add)
        next
          case False
          then show ?thesis
            by (auto simp add: S2_def SK_simp K_def_alt)
        qed
        done

      (* moreover have "(\<Sum>k<length p. if issub (p ! k) \<and> l = modifies (p ! k) then ZL ic p (B c) (Suc t) l && SK ic p (B c) (Suc t) k else 0)
                   = (\<Sum>k<length p. ?)" *)

        ultimately show ?thesis using Add Suc unfolding assms
        (* apply (auto simp add: RL_simp ZL_simp SK_simp ic simp del: steps.simps digit_binary_mult.simps cong del: if_weak_cong) *)
        sorry
    next
      case (Sub x21 x22 x23)
      then show ?thesis sorry
    next
      case Halt
      then show ?thesis sorry
    qed
  qed
qed

lemma lm04_23_multiple_register_1:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
    and a :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  assumes "l = 0"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == length p"
  defines "rl == RL q ((register_rows cl)!l) b"
      and "zl \<equiv> ZL q ((zero_rows cl)!l) b"
      and "sk == \<lambda>k. SK q ((state_rows cl p)!k) b"
    shows "rl =  a + b*rl + b * (\<Sum>k<m. (if isadd (p!k) \<and> l = modifies (p!k) then sk k else 0))
                          - b * (\<Sum>k<m. (if issub (p!k) \<and> l = modifies (p!k) then zl && sk k else 0))"
proof -
  show ?thesis sorry
qed

(* [T] 4.24 for states *)
lemma lm04_24_multiple_states:
  fixes c :: nat
    and d :: state
    and ic :: configuration
    and p :: program
    and q :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  assumes "d > 0"
  assumes "register_machine_check ic p = True"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == length p"
  defines "e == E q b"
  defines "sd == SK q ((state_rows cl p)!d) b"
      and "sk == \<lambda>k. SK q ((state_rows cl p)!k) b"
      and "zl == \<lambda>k. (ZL q ((zero_rows cl)!(modifies (p!k))) b)"
    shows "sd =   b * (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then sk k else 0))
                + b * (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then sk k && zl k else 0))   
                + b * (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then sk k && (e - zl k) else 0))"
proof -
  define sadd where "sadd \<equiv> (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then sk k else 0))"
  define ssub where "ssub \<equiv> (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then sk k && zl k else 0))"
  define salt where "salt \<equiv> (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then sk k && (e - zl k) else 0))"

  define skt where "skt \<equiv> \<lambda>t. digit sd b t" (* t-th digit of sd in base b *)
  define skt' where "skt' \<equiv> \<lambda>t. digit sd b (t+1)" (* t+1st digit of sd in base b *)
  define zlt where "zlt \<equiv> \<lambda>t k. digit (zl k) b t" (* t-th digit of zl in base b *)

  define a1 where "a1 \<equiv> \<lambda>t. (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then skt t && (zlt t k) else 0))"
  define a2 where "a2 \<equiv> \<lambda>t. (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then skt t && zlt t k else 0))"
  define a3 where "a3 \<equiv> \<lambda>t. (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then skt t && (1 - zlt t k) else 0))"

  from lm04_07_one_step_relation_state
  have sum: "\<And>t. skt' t = a1 t + a2 t + a3 t"
    sorry
  from sum have "\<And>t. b^Suc t * skt' t = b^Suc t * a1 t + b^Suc t * a2 t + b^Suc t * a3 t"
    by (simp add: distrib_left)
  from this have "(\<Sum>t<q. b^Suc t * skt' t) = (\<Sum>t<q. b^Suc t * a1 t + b^Suc t * a2 t + b^Suc t * a3 t)"
    by auto
  also from sum have "\<And>t t'::nat. b^Suc t * (skt' t + skt' t') = b^Suc t * (a1 t + a1 t' + a2 t + a2 t' + a3 t + a3 t')"
    by auto
  moreover have "b * sd = (\<Sum>t<q. b^Suc t * skt' t)" sorry
  (*ultimately have "b * s" *)

  (* Finally: *)
  have "sd = b * (sadd + ssub + salt)" sorry
  thus ?thesis
    by (simp add: sadd_def salt_def semiring_normalization_rules(34) ssub_def)
qed

(* [T] 4.25 for states *)
lemma lm04_25_multiple_states_1:
  fixes c :: nat
    and d :: state
    and ic :: configuration
    and p :: program
    and q :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  assumes "d = 0"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == length p"
  defines "e == E q b"
  defines "sd == SK q ((state_rows cl p)!d) b"
      and "sk == \<lambda>k. SK q ((state_rows cl p)!k) b"
    shows "sd = 1 +  b * (\<Sum>k<m. (if isadd (p!k) \<and> d = goes_to (p!k) then sk k else 0))
                  + b * (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to (p!k) then sk k && (ZL q ((zero_rows cl)!(modifies (p!k))) b) else 0))   
                  + b * (\<Sum>k<m. (if issub (p!k) \<and> d = goes_to_alt (p!k) then sk k && (e - (ZL q ((zero_rows cl)!(modifies (p!k))) b)) else 0))"
proof -
  fix k (* ? *)
  consider (add) "isadd (p!k)" | (sub) "issub (p!k)" | (halt) "issub (p!k)" sorry
  thus ?thesis sorry
qed



(* STILL TO BE DONE: *)

(* Next 3 lemmas use conversion from columns to rows, specifically need more assumptions:
 * - termination of register machine
 * - initial conditions
 * - one step relations
 *)

(* [T] 4.15 *)
lemma lm04_15_d_masks_registers:
  fixes l::register
    and ic::configuration
    and p::program
    and a::nat (* input *)
    and c::nat
  defines "s \<equiv> fst ic"
      and "tape \<equiv> snd ic"

  defines "m \<equiv> length p"
      and "tape' \<equiv> snd (step ic p)"

  assumes is_val: "is_valid ic p a"
      and "terminates ic p q"

  (* to be exported to a function that verifies this *)
  assumes "\<And>l t :: nat. (l < n & t \<le> q) \<longrightarrow> (2^c > rlt t l & 2^c > zlt t l)"
  assumes "\<And>k t :: nat. (k < m & t \<le> q) \<longrightarrow> (2^c > skt t k)"
  assumes "c > 0" (* needs to be proven from the above *)

  defines defB: "b \<equiv> B c"
      and "cl \<equiv> run ic p q"

  defines "d \<equiv> D q c b"
      and "rl \<equiv> RL q ((register_rows cl)!l) b"
    shows "masks (nat d) rl" (is "?P c")
proof -
  show ?thesis
    using is_val unfolding assms
    sorry
qed

(* [T] 4.17 *)
lemma lm04_17_masks_zeros:
  fixes c::nat
    and l::register (* or equivalently to zero-indicator values *)
    and cl::"configuration list" (* cannot be assumed, needs `run` fct from initial cond. *)
  defines "b \<equiv> B c"
      and "q \<equiv> length cl"
  defines "e \<equiv> E q b"
      and "zl \<equiv> ZL q ((zero_rows cl)!l) b"
  shows "masks e zl"
proof-
  show ?thesis sorry
qed

(* [T] 4.19 -- 4.20 *)
lemma lm04_19_relation_register_zeros:
  fixes c::nat
    and l::register
    and cl::"configuration list" (* cannot be assumed, needs `run` fct from initial cond. *)
  defines "b \<equiv> B c"
      and "q \<equiv> length cl"
  defines "f \<equiv> F q c b"
      and "d \<equiv> D q c b"
      and "zl \<equiv> ZL q ((zero_rows cl)!l) b"
      and "rl \<equiv> RL q ((register_rows cl)!l) b"
    shows "2^c * zl = (rl + nat d) && f"
proof -
  have "2^c * zl = (rl + 2^c - 1) && 2^c" sorry (* 4.19 *)
  then show ?thesis sorry (* 4.20 *)
qed


subsection {* Halting conditions / Wrapping up *}

(* [4.8] *)
(* THE HALTING CONDITION *)
lemma lm04_08_halting:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == (length p)"
  defines "s == state_rows cl p"
  shows "(s ! m ! q) = 1"
proof-
  show ?thesis sorry
qed

lemma lm04_09_halting:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
    and t :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == (length p)"
  defines "s == state_rows cl p"
  assumes y
  shows "t \<noteq> q --> (s ! m ! t) = 0"
proof-
  show ?thesis sorry
qed

lemma lm04_26_halting:
  fixes c :: nat
    and l :: register
    and ic :: configuration
    and p :: program
    and q :: nat
    and t :: nat
  (* ASSUMPTION: IT TERMINATES *)
  assumes terminate: "Halt = p ! (fst (steps ic p q)) \<and> (\<forall>x<q. Halt \<noteq> p ! (fst (steps ic p x)))"
  defines "b == B c"
      and "cl == run ic p q"
      and "m == (length p)"
  defines "s == state_rows cl p"
  assumes y
  defines "sm == (SK q (s ! m) b)"
  shows "sm = b^q"
proof-
  from sm_def cl_def lm04_09_halting m_def s_def terminate 
    have st1: "(ALL x < q. s ! m ! x = 0)" by auto
  from cl_def lm04_08_halting m_def s_def terminate 
    have st2: "(s ! m ! q) = 1" by blast
  from st1 st2 sm_def show ?thesis sorry
qed

end
