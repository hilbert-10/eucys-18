(*
 * 2.4
 * POSITIONAL NOTATION
 * Helper Methods
 *)

theory CH2PositionalNotation
imports Main List 
begin

(* NOTE ON NAMING CONVENTIONS:
 * - helper methods prefixed with `hl_`
 * - we prefix functions also defined for natural numbers with `pn` (this 
 *   also applies to functions with common names like `val`, `convert`, etc)
 *)

text \<open>
  We describe a number in the positional notation using the base and the list
  of digits. We also implement functions that convert a number represented in
  the positional notation to a natural number.
\<close>
datatype pos = Pos nat "nat list"

text \<open>
  Parts of the proof relies on extracting the $k$-th digit from a number, so we
  implement two digit functions \texttt{digit} and \texttt{pndigit} that
  respectively work with natural numbers and numbers represented in the
  Positional notation.
\<close>
(* return the k-th digit of the number *)
fun pndigit :: "pos => nat => nat"
  where
    "(pndigit (Pos base digits) k) = (if (k < (length digits))
                                      then digits!k
                                      else 0)"

(* zero-indexed, corresponds to powers of base *)
fun digit :: "nat => nat => nat => nat"
  where
    "(digit num base k) = ((num mod (base^(Suc k))) div (base^k))"

(* Alternative and equivalent digit function, by Scott Deng *)
(* This function might, at times, be easier to use in proofs *)
fun digit' :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat"
  where "digit' num b d = (num div (b ^ d)) mod b"

(* Proof of equivalence of both digit functions, by Scott Deng *)
lemma equivalence_for_digit_function[simp]:
  fixes num::nat
  fixes base::nat
  assumes "base \<ge> 2"
  fixes d::nat
  shows "digit' num base d = digit num base d"
proof -
  have "num div (base ^ d) mod base = num mod (base^(Suc d)) div base^d"
    by (metis (no_types, lifting) Divides.mod_mult2_eq One_nat_def Suc_eq_plus1 add.right_neutral assms div_mult_self4 less_SucI less_one linorder_not_le mod_div_trivial numerals(2) power_Suc0_right power_add power_not_zero)
  thus ?thesis by simp
qed

text \<open>
  To convert a number represented in the positional notation back to a natural
  number, we use the formula
    $$x = \sum_{i = 0}^n b^i d_i$$
  where $b$ is the base, $d_i$ is the $i$-th digit of the number, and $n$ is 
  the total number of digits.
\<close>

(* Convert a natural number to its positional notation representation *)
fun hl_digit_list :: "nat => nat => nat list"
  where
    "(hl_digit_list 0 _) = []"
  | "(hl_digit_list _ 0) = []"
  | "(hl_digit_list _ (Suc 0)) = []" 
  | "(hl_digit_list n base) = (n mod base) # (hl_digit_list (n div base) base)"

(* Return the natural number represented by the positional notation encoding *)
fun hl_pnval :: "nat => nat list => nat" 
  where 
    "(hl_pnval _ []) = 0"
  | "(hl_pnval base (d # ds)) = d + (base * (hl_pnval base ds))"

lemma hl_pnval_consistency:
  fixes a :: nat
  shows "a = hl_pnval 2 (hl_digit_list a 2)" sorry

fun pnval :: "pos => nat" 
  where
    "(pnval (Pos base digits)) = (hl_pnval base digits)"

fun pnconvert :: "nat => nat => pos"
  where
    "(pnconvert 0 base) = (Pos base [0])"
  | "(pnconvert n base) = (Pos base (hl_digit_list n base))"

(* remove trailing zeroes from a list of digits *)
fun hl_clean_zeroes :: "nat list => nat list"
  where
    "(hl_clean_zeroes []) = []"
  | "(hl_clean_zeroes digits) = (if ((last digits) = 0 & (length digits) > 1)
                                then (hl_clean_zeroes (butlast digits))
                                else digits)"

(* we use this as an assumption in many of the proofs *)
fun pn_digit_smaller_than_base :: "pos => bool"
  where
    "(pn_digit_smaller_than_base (Pos base digits)) =
        (fold (% a b. a & b) (map (%a. a < base) digits) True)"

(* Proof of this lemma work-in-progress by Bogdan Ciurezu *)
lemma pn_consistency:
  fixes digits :: "nat list"
  fixes base :: nat
  assumes "base > 1"
  assumes "digits ~= []"
  assumes "pn_digit_smaller_than_base (Pos base digits)"
  shows "(n = pnval (Pos base digits))
          = ((Pos base (hl_clean_zeroes digits)) = (pnconvert n base))"
        (is "?P = ?Q")
proof -
  show ?thesis
    unfolding assms
    sorry
qed

lemma pn_digit_equivalence:
  fixes digits :: "nat list"
  fixes base k :: nat
  assumes "base > 1"
  assumes "pn_digit_smaller_than_base (Pos base digits)"
  shows "(d = digit (pnval (Pos base digits)) base k) = (d = (digits!k))"
        (is "?P = ?Q")
  (* proof
    assume "P"
    ...
    show "Q"
  next
    assume "Q"
    ...
    show "P"
  qed *)
  sorry

end