theory CH3ExponentialDiophantine
  imports Main Complex
begin

(* SOME EASY DEFINITIONS DONE BY ALL GROUP MEMBERS
                ll. 5 - 68                         *)

(* definition of exponential Diophantine polynomials *)
datatype D = const nat | var nat | sum D D | prod D D | exp D D

(* definition of 2x2 matrices *)
datatype mat2 = mat (mat_11 : int) (mat_12 : int) (mat_21 : int) (mat_22 : int)               

(* some operations on matrices*)
fun mat_plus:: "mat2 \<Rightarrow> mat2 \<Rightarrow> mat2"
  where
"mat_plus (mat m1 m2 m3 m4) (mat n1 n2 n3 n4) = mat (m1 + n1) (m2 + n2) (m3 + n3) (m4 + n4)"

fun mat_mul:: "mat2 \<Rightarrow> mat2 \<Rightarrow> mat2"
  where
"mat_mul (mat m1 m2 m3 m4) (mat n1 n2 n3 n4) = mat (m1 * n1 + m2 * n3) (m1 * n2 + m2 * n4) (m3 * n1 + m4 * n3) (m3 * n2 + m4 * n4)"

fun mat_pow:: "nat \<Rightarrow> mat2 \<Rightarrow> mat2"
  where
"mat_pow 0 _ = mat 1 0 0 1"
| "mat_pow n A = mat_mul A (mat_pow (n - 1) A)"

fun mat_det::"mat2 \<Rightarrow> int"
  where
"mat_det (mat a b c d) = a * d - b * c"

fun mat_int::"int \<Rightarrow> mat2 \<Rightarrow> mat2"
  where
"mat_int a (mat m1 m2 m3 m4) = mat (a*m1) (a*m2) (a*m3) (a*m4)"

fun mat_minus:: "mat2 \<Rightarrow> mat2 \<Rightarrow> mat2"
  where
"mat_minus (mat m1 m2 m3 m4) (mat n1 n2 n3 n4) = mat (m1 - n1) (m2 - n2) (m3 - n3) (m4 - n4)"

(* definition 3.2 *)
fun alpha:: "nat \<Rightarrow> nat \<Rightarrow> int"
  where
    "alpha b 0 = 0"
  | "alpha b (Suc 0) = 1"
  |alpha_n: "alpha b (Suc (Suc n)) = (int b) * (alpha b (Suc n)) - (alpha b n)"

(* definition 3.7 *)
fun A :: "nat \<Rightarrow> nat \<Rightarrow> mat2"
  where
"A b 0 = mat 1 0 0 1"
| "A b n = mat (alpha b (n + 1)) (-(alpha b n)) (alpha b n) (-(alpha b (n - 1)))"

(* definition 3.9 for B_b *)
fun B :: "nat \<Rightarrow> mat2"
  where
    "B b = mat (int b) (-1) 1 0"

(* value of a polynomial given a valuation function *)
fun val:: "(nat \<Rightarrow> nat) \<Rightarrow> D \<Rightarrow> nat"
  where
  "val f (const c) = c"
  | "val f (var x) = f x"
  | "val f (sum p q) = (val f p) + (val f q)"
  | "val f (prod p q) = (val f p) * (val f q)"
  | "val f (exp p q) = (val f q) ^ (val f p)"


(* FOUR LEMMAS (PROVED BY OTHER GROUP MEMBERS)
                  ll. 68 - 130                      *)

lemma alpha_increasing:
  fixes b n::nat
  assumes assumption: "int b > 2"
  shows "alpha b n < alpha b (Suc n) \<and> 0 < alpha b (Suc n)"
sorry

(* 3.4 *)
lemma alpha_superlinear:
  fixes b n::nat
  assumes assumption: "b > 2"
  shows "int n \<le> alpha b n"
sorry

(* 3.5 *)
lemma alpha_linear:
  fixes n::nat
  shows "alpha 2 n = n"
sorry

(* 3.7 lemma*)
lemma A_id:
"\<forall>b. A b 0 = mat 1 0 0 1" by auto

(* 3.10 *)
lemma A_pow:
"\<forall>b. \<forall>n. A b (n) = mat_pow n (mat (int b) (-1) 1 0)"
sorry

(* 3.11 *)
lemma alpha_det2:
fixes n b :: nat
assumes "b>2" "n>0"
(* for n=0, isabelle detects counterexample, possibly because alpha is defined on natural numbers only *)
shows "(alpha b (n-1))^2 - (int b) * (alpha b (n-1) * (alpha b n)) + (alpha b n)^2 = 1"
sorry

(* 3.12 *)
lemma alpha_det1:
fixes n b :: nat
assumes "b>2"
shows "(alpha b (n+1))^2 - (int b) * (alpha b (n+1) * (alpha b n)) + (alpha b n)^2 = 1"
sorry

(*lemma for 3.10*)
lemma pow_one[simp]:
  fixes b n :: nat
  assumes "b>2"
  shows "mat_pow 1 (B b) = (B b)" by simp

(* lemma for 3.29 *)
lemma mkl:
  assumes S0: "b>2" "k>0"
  shows "alpha b k dvd alpha b m \<longleftrightarrow> (\<exists>l. k*l=m)"
sorry

(* another lemma for 3.29 *)
lemma divisibility_equations:
  assumes 0: "m = k*l" and "b>2" "m>0"
  shows "A b m = mat_pow l (mat_minus (mat_int (alpha b k) (B b)) (mat_int (alpha b (k-1)) (mat 1 0 0 1)))"
sorry

(* NOW: BENEDIKT'S WORK (ONLY): ll. 132-(-1)
  properties of 2x2 matrices   *)

lemma mat_neutral_element:
  fixes a b c d :: int
  defines "N \<equiv> mat a b c d"
  defines "E \<equiv> mat 1 0 0 1"
  shows "mat_mul E N = N" by (simp add: assms)

lemma mat_associativity:
  fixes a b c d e f g h i j k l:: int
  defines "G \<equiv> mat a b c d"
  defines "D \<equiv> mat e f g h"
  defines "C \<equiv> mat i j k l"
  shows "mat_mul (mat_mul G D) C = mat_mul G (mat_mul D C)"
proof -
  from D_def G_def have GD: "mat_mul G D = mat (a * e + b * g) (a * f + b * h) (c * e + d * g) (c * f + d * h)" by simp
  from C_def GD have GDCL: "mat_mul (mat_mul G D) C = mat ((a * e + b * g)*i+(a * f + b * h)*k) ((a * e + b * g)*j+(a * f + b * h)*l)
    ((c * e + d * g)*i+(c * f + d * h)*k) ((c * e + d * g)*j+(c * f + d * h)*l)" by simp
  have m11: "(a * e + b * g)*i+(a * f + b * h)*k = a* (e * i + f * k) + b * (g * i + h * k)" by algebra
  have m12: "(a * e + b * g)*j+(a * f + b * h)*l = a * (e * j + f * l) + b * (g * j + h * l)" by algebra
  have m21: "(c * e + d * g)*i+(c * f + d * h)*k = c * (e * i + f * k) + d * (g * i + h * k)" by algebra
  have m22: "(c * e + d * g)*j+(c * f + d * h)*l = c * (e * j + f * l) + d * (g * j + h * l)" by algebra
  from D_def C_def have DC: "mat_mul D C = mat (e * i + f * k) (e * j + f * l) (g * i + h * k) (g * j + h * l)" by simp
  from G_def DC have GDCR: "mat_mul G (mat_mul D C) = mat (a* (e * i + f * k) + b * (g * i + h * k)) (a * (e * j + f * l) + b * (g * j + h * l))
    (c * (e * i + f * k) + d * (g * i + h * k)) (c * (e * j + f * l) + d * (g * j + h * l))" by simp
  from GDCL m11 m12 m21 m22 GDCR show "mat_mul (mat_mul G D) C = mat_mul G (mat_mul D C)" by simp
qed

lemma mat_exp_law:
  fixes n m :: nat
  shows "mat_mul (mat_pow n M) (mat_pow m M) = mat_pow (n+m) M" (is "?P n")
proof(induction n)
  have E0: "(mat_pow 0 M) = mat 1 0 0 1" by simp
  have N0: "mat_mul (mat 1 0 0 1) (mat_pow m M) = (mat_pow m M)" by (metis mat2.exhaust mat_neutral_element)
  from N0 show "?P 0" by auto
next
  fix n assume IH: "?P n"
  have S1: "mat_mul (mat_pow (Suc(n)) M) (mat_pow m M) = mat_mul (mat_mul M (mat_pow n M)) (mat_pow m M)" by simp
  from mat_associativity have S2: "mat_mul (mat_mul M (mat_pow n M)) (mat_pow m M) = mat_mul M (mat_mul (mat_pow n M) (mat_pow m M))"
    by (metis mat2.exhaust mat_associativity)
  have S4: "mat_mul M (mat_pow (n+m) M) = mat_pow (Suc(n)+m) M" by simp
  from IH S1 S2 S4 show PF: "?P(Suc n)" by presburger
qed

lemma mat_exp_law_mult:
  fixes n m :: nat
  shows "mat_pow (n*m) M = mat_pow n (mat_pow m M)" (is "?P n")
proof(induct n)
  show "?P 0" by auto
next
  fix n assume IH: "?P n"
  show "?P (Suc(n))" by (metis IH diff_Suc_1 mat_exp_law mat_pow.simps(2) mult_Suc)
qed

(* basic property, analogous for the other 2*)

lemma mat_11_mul:
  fixes a b c d e f g h :: int
  defines "D \<equiv> mat a b c d"
  defines "C \<equiv> mat e f g h"
  shows "mat_11 (mat_mul D C) = mat_11 D * mat_11 C + mat_12 D * mat_21 C" by (simp add: D_def C_def)

lemma mat_21_mul:
  fixes a b c d e f g h :: int
  defines "D \<equiv> mat a b c d"
  defines "C \<equiv> mat e f g h"
  shows "mat_21 (mat_mul D C) = mat_21 D * mat_11 C + mat_22 D * mat_21 C" by (simp add: D_def C_def)

lemma mat_22_mul:
  fixes B1 C :: mat2
  shows "mat_22 (mat_mul B1 C) = mat_21 B1 * mat_12 C + mat_22 B1 * mat_22 C" by (metis mat2.exhaust_sel mat2.sel(4) mat_mul.simps)

(* Section 3.4*)
(*3.24*)
lemma representation:
  fixes k m :: nat
  assumes "k > 0"
  "n = m mod k"
  "l = (m-n)div k"
  shows "m = n+k*l \<and> 0\<le>n \<and> n\<le>k-1" by (metis Suc_pred' assms(1) assms(2) assms(3)
    le_add2 le_add_same_cancel2 less_Suc_eq_le minus_mod_eq_div_mult minus_mod_eq_mult_div mod_div_mult_eq
    mod_less_divisor neq0_conv nonzero_mult_div_cancel_left)

(* lemma needed for 3.25 *)
lemma div_3251:
  fixes b k m:: nat
  assumes "b>2" and "k>0"
  defines "n \<equiv> m mod k"
  defines "l \<equiv> (m-n) div k"
  shows "A b m = mat_mul (A b n) (mat_pow l (A b k))"
proof -
  from assms(2) l_def n_def representation have m: "m = n+k*l \<and> 0\<le>n \<and> n\<le>k-1" by simp
  from A_pow assms(1) have Abm2: "A b m = mat_pow m (B b)" by simp
  from m have Bm: "mat_pow m (B b) =mat_pow (n+k*l) (B b)" by simp
  from mat_exp_law have as1: "mat_pow (n+k*l) (B b) = mat_mul (mat_pow n (B b)) (mat_pow (k*l) (B b))" by simp
  from mat_exp_law_mult have as2: "mat_pow (k*l) (B b) = mat_pow l (mat_pow k (B b))" by (metis mult.commute)
  from A_pow assms have Abn: "mat_pow n (B b) = A b n" by simp
  from A_pow assms(1) have Ablk: "mat_pow l (mat_pow k (B b)) = mat_pow l (A b k)" by simp
  from Ablk Abm2 Abn Bm as1 as2 show Abm: "A b m = mat_mul (A b n) (mat_pow l (A b k))" by simp
qed

(* another lemma needed for 3.25*)
lemma div_3252:
  fixes a b c d m:: int and l :: nat
  defines "M \<equiv> mat a b c d"
  assumes "mat_21 M mod m = 0"
  shows "(mat_21 (mat_pow l M)) mod m = 0 " (is "?P l")
proof(induction l)
  show "?P 0" by simp
next
  fix l assume IH: "?P l"
  define Ml where "Ml = mat_pow l M"
  have S1: "mat_pow (Suc(l)) M = mat_mul M (mat_pow l M)" by simp
  from mat_21_mul have S2: "mat_21 (mat_mul M Ml) = mat_21 M * mat_11 Ml + mat_22 M * mat_21 Ml" by (rule_tac mat_mul.induct mat_plus.induct)
  from S1 S2 Ml_def have S3: "mat_21 (mat_pow (Suc(l)) M) = mat_21 M * mat_11 Ml + mat_22 M * mat_21 Ml" by simp
  from assms(2) have S4: "(mat_21 M * mat_11 Ml) mod m = 0" by auto
  from IH Ml_def have S5: " mat_22 M * mat_21 Ml mod m = 0" by auto
  from S4 S5 have S6: "(mat_21 M * mat_11 Ml + mat_22 M * mat_21 Ml) mod m = 0" by auto
  from S3 S6 show "?P (Suc(l))" by simp
qed

(* and another lemma needed for 3.25*)
lemma div_3253:
  fixes a b c d m:: int and l :: nat
  defines "M \<equiv> mat a b c d"
  assumes "mat_21 M mod m = 0"
  shows "((mat_11 (mat_pow l M)) - a^l) mod m = 0" (is "?P l")
proof(induction l)
  show "?P 0" by simp
next
  fix l assume IH: "?P l"
  define Ml where "Ml = mat_pow l M"
  from Ml_def have S1: "mat_pow (Suc(l)) M = mat_mul M Ml" by simp
  from mat_11_mul have S2: "mat_11 (mat_mul M Ml) = mat_11 M * mat_11 Ml + mat_12 M * mat_21 Ml" by (rule_tac mat_mul.induct mat_plus.induct)
  from S1 S2 have S3: "mat_11 (mat_pow (Suc(l)) M) = mat_11 M * mat_11 Ml + mat_12 M * mat_21 Ml" by simp
  from M_def Ml_def assms(2) div_3252 have S4: "mat_21 Ml mod m = 0" by auto
  from IH Ml_def have S5: "(mat_11 Ml - a ^ l) mod m = 0" by auto
  from IH M_def have S6: "(mat_11 M -a) mod m = 0" by simp
  from S4 have S7: "(mat_12 M * mat_21 Ml) mod m = 0" by auto
  from S5 S6 have S8: "(mat_11 M * mat_11 Ml- a^(Suc(l))) mod m = 0" 
    by (metis M_def mat2.sel(1) mod_0 mod_mult_right_eq mult_zero_right power_Suc right_diff_distrib)
  from S7 S8 have S9: "(mat_11 M * mat_11 Ml - a^(Suc(l)) + mat_12 M * mat_21 Ml ) mod m = 0" by auto
  from S9 have S10: "(mat_11 M * mat_11 Ml + mat_12 M * mat_21 Ml - a^(Suc(l))) mod m = 0" by smt
  from S3 S10 show "?P (Suc(l))" by auto
qed

(* 3.25 = 3.26 *)
lemma divisibility_lemma1:
  fixes b k m:: nat
  assumes "b>2" and "k>0"
  defines "n \<equiv> m mod k"
  defines "l \<equiv> (m-n) div k"
  shows "((alpha b m mod (alpha b k)) = ((alpha b n) * (alpha b (k+1)) ^ l) mod (alpha b k))"
proof -
  from assms(2) l_def n_def representation have m: "m = n+k*l \<and> 0\<le>n \<and> n\<le>k-1" by simp
  consider (eq0) "n = 0" | (neq0) "n > 0" by auto
  then show ?thesis
  proof cases
    case eq0
    from assms(1) assms(2) div_3251 l_def n_def have Abm_gen: "A b m = mat_mul (A b n) (mat_pow l (A b k))" by blast
    from assms(2) neq0_conv have Abk: "mat_pow l (A b k) = mat_pow l (mat (alpha b (k+1)) (-alpha b k) (alpha b k) (-alpha b (k-1)))" by (metis A.elims)
    from eq0 have Abm: "A b m = mat_pow l (mat (alpha b (k+1)) (-alpha b k) (alpha b k) (-alpha b (k-1)))"
      by (metis A_id Abk Abm_gen add.left_neutral mat_exp_law mat_pow.simps(1))
    have Abm1: "mat_21 (A b m) = alpha b m" by (metis A.elims alpha.simps(1) mat2.sel(3))
    from Abm div_3252 have Abm2: "mat_21 (mat_pow l (mat (alpha b (k+1)) (-alpha b k) (alpha b k) (-alpha b (k-1)))) mod (alpha b k) = 0" by simp
    from Abm Abm1 Abm2 have MR0: "alpha b m mod alpha b k = 0" by simp
    from MR0 eq0 show ?thesis by simp
  next case neq0
      from assms(1) assms(2) div_3251 l_def n_def have Abm_gen: "A b m = mat_mul (A b n) (mat_pow l (A b k))" by blast
  from assms(2) neq0_conv have Abk: "mat_pow l (A b k) = mat_pow l (mat (alpha b (k+1)) (-alpha b k) (alpha b k) (-alpha b (k-1)))" by (metis A.elims)
  from n_def neq0 have N0: "n>0" by simp
  define M where "M = mat (alpha b (n + 1)) (-(alpha b n)) (alpha b n) (-(alpha b (n - 1)))"
  define N where "N = mat_pow l (mat (alpha b (k+1)) (-alpha b k) (alpha b k) (-alpha b (k-1)))"
  from Suc_pred' neq0 have Abn: "A b n = mat (alpha b (n + 1)) (-(alpha b n)) (alpha b n) (-(alpha b (n - 1)))" by (metis A.elims neq0_conv)
  from Abm_gen Abn Abk M_def N_def have Abm: "A b m = mat_mul M N" by simp
  (* substitutions done! next: calculations *)
  from Abm mat_21_mul have S1: "mat_21 (mat_mul M N) = mat_21 M * mat_11 N + mat_22 M * mat_21 N" by (rule_tac mat_mul.induct mat_plus.induct)
  have S2: "mat_21 (A b m) = alpha b m" by (metis A.elims alpha.simps(1) mat2.sel(3))
  from S1 S2 Abm have S3: "alpha b m = mat_21 M * mat_11 N + mat_22 M * mat_21 N" by simp
  from S3 have S4: "(alpha b m - (mat_21 M * mat_11 N + mat_22 M * mat_21 N)) mod (alpha b k) = 0" by simp
  from M_def have S5: "mat_21 M = alpha b n" by simp
  from div_3253 N_def have S6: "(mat_11 N - (alpha b (k+1)) ^ l) mod (alpha b k) = 0" by simp
  from N_def Abm div_3252 have S7: "mat_21 N mod (alpha b k) = 0" by simp
  from S4 S7 have S8: "(alpha b m - mat_21 M * mat_11 N) mod (alpha b k) = 0" by algebra
  from S5 S6 have S9: "(mat_21 M * mat_11 N - (alpha b n) * (alpha b (k+1)) ^ l) mod (alpha b k) = 0"
    by (metis mod_0 mod_mult_left_eq mult.commute mult_zero_left right_diff_distrib')
  from S8 S9 show ?thesis by (metis (no_types, hide_lams) diff_add_cancel semiring_div_class.mod_mult_self4 zmod_eq_0D)
  qed
qed

(*lemma needed for 3.27*)

lemma div_coprime:
fixes b k l :: nat
assumes "b>2" "n>=0"
shows "coprime (alpha b k) (alpha b (k+1))" (is "?P")
proof(rule ccontr)
  assume as: "\<not> ?P"
  define n where "n = gcd (alpha b k) (alpha b (k+1))"
  from n_def have S1: "n > 1" by (smt Nat_Transfer.transfer_nat_int_function_closures(9)
    Suc_eq_plus1 alpha_superlinear as assms(1) gcd_eq_0_iff gcd_ge_0_int semiring_1_class.of_nat_simps(2))
  from alpha_det1 assms have S2: "(alpha b (k+1))^2 - (int b) * (alpha b (k+1) * (alpha b k)) + (alpha b k)^2 = 1" by blast
  from n_def have D1: " n dvd (alpha b (k+1))^2" by (simp add: numeral_2_eq_2)
  from n_def have D2: " n dvd (- (int b) * (alpha b (k+1) * (alpha b k)))" by simp
  from n_def have D3: "n dvd (alpha b k)^2" by (simp add: gcd_dvdI1)
  from D1 D2 D3 have S3: "n dvd ((alpha b (k+1))^2 - (int b) * (alpha b (k+1) * (alpha b k)) + (alpha b k)^2)" by simp
  from S2 S3 have S4: "n dvd 1" by simp
  from S4 n_def as is_unit_gcd show "False" by blast
qed

(* 3.27 *)
lemma divisibility_lemma2:
  fixes b k m:: nat
  assumes "b>2" and "k>0"
  defines "n \<equiv> m mod k"
  defines "l \<equiv> (m-n) div k"
  assumes "alpha b m mod alpha b k = 0"
  shows "(alpha b n) mod (alpha b k) = 0"
proof -
  from assms(2) l_def n_def representation have m: "0\<le>n \<and> n\<le>k-1" by simp
  from divisibility_lemma1 assms(1) assms(2) l_def n_def have S1: "(alpha b m) mod (alpha b k)  = (alpha b n) * (alpha b (k+1)) ^ l mod (alpha b k)" by blast
  from S1 assms(5) have S2: "(alpha b k) dvd ((alpha b n) * (alpha b (k+1)) ^ l)" by auto
  from S1 div_coprime S2 assms(1) coprime_dvd_mult coprime_exp have S3: "(alpha b k) dvd (alpha b n)" by blast
  from S3 mod_eq_0_iff_dvd show "(alpha b n) mod (alpha b k) = 0" by blast
qed


(* 3.23 MAIN RESULT *)
theorem divisibility_alpha:
  fixes b k m:: nat
  assumes "b>2" and "k>0"
  shows "alpha b m mod alpha b k = 0 \<longleftrightarrow> m mod k = 0" (is "?P \<longleftrightarrow> ?Q")
proof
  assume Q: "?Q"
    define n where "n = m mod k"
    have N: "n=0" by (simp add: Q n_def)
    from N have Abn: "alpha b n = 0" by simp
    from Abn divisibility_lemma1 assms(1) assms(2) mult_eq_0_iff n_def show "?P" by simp
  next assume P: "?P"
  define n where "n = m mod k"
  define l where "l = (m-n) div k"
  define B where "B = (mat (int b) (-1) 1 0)"
  from assms(2) l_def n_def representation have m: "0\<le>n \<and> n\<le>k-1" by simp
  from divisibility_lemma2 assms(1) assms(2) n_def P have S1: "(alpha b n) mod (alpha b k) = 0" by simp
  from alpha_increasing m have S2: "alpha b n < alpha b k" by (metis assms(1) assms(2) less_imp_of_nat_less lift_Suc_mono_less_iff
    n_def semiring_numeral_div_class.pos_mod_bound transfer_int_nat_numerals(3))
  from S1 S2 have S3: "n=0" by (smt alpha_superlinear assms(1) mod_pos_pos_trivial neq0_conv of_nat_0_less_iff)
  from S3 n_def show "?Q" by simp
  qed

(* Section 3.5 *)

lemma divisibility_cong:
  fixes e f :: int
  fixes l :: nat
  fixes M :: mat2
  assumes "mat_22 M = 0" "mat_21 M = 1"
  defines "E == mat (int 1) (int 0) (int 0) (int 1)"
  shows "(mat_21 (mat_pow l (mat_minus (mat_int e M) (mat_int f E)))) mod e^2 = (-1)^(l-1)*l*e*f^(l-1)*(mat_21 M) mod e^2 
    \<and> mat_22  (mat_pow l (mat_minus (mat_int e M) (mat_int f E))) mod e^2 = (-1)^l *f^l mod e^2" (is "?P l \<and> ?Q l" )
proof(induction l)
  case 0
  then show ?case by simp
next
  case (Suc l)
  from mat_exp_law Suc_eq_plus1 have S0: "mat_pow (Suc(l)) (mat_minus (mat_int e M) (mat_int f E)) = 
    mat_mul (mat_pow l (mat_minus (mat_int e M) (mat_int f E))) (mat_pow 1 (mat_minus (mat_int e M) (mat_int f E)))" by presburger
  have S1: "mat_pow 1 (mat_minus (mat_int e M) (mat_int f E)) = mat_minus (mat_int e M) (mat_int f E)" by (smt One_nat_def 
    diff_self_eq_0 mat2.exhaust_sel mat2.sel(1) mat2.sel(4) mat_mul.simps mat_pow.simps(1) mat_pow.simps(2)
    mult_cancel_left2 mult_cancel_right2)
  from S0 S1 have S2: "mat_pow (Suc(l)) (mat_minus (mat_int e M) (mat_int f E)) = 
    mat_mul (mat_pow l (mat_minus (mat_int e M) (mat_int f E))) (mat_minus (mat_int e M) (mat_int f E))" by simp_all
  define a1 where "a1 = mat_11 (mat_minus (mat_int e M) (mat_int f E))"
  define b1 where "b1 = mat_12 (mat_minus (mat_int e M) (mat_int f E))"
  define c1 where "c1 = mat_21 (mat_minus (mat_int e M) (mat_int f E))"
  define d1 where "d1 = mat_22 (mat_minus (mat_int e M) (mat_int f E))"
  define a where "a = mat_11 M"
  define b where "b = mat_12 M"
  define c where "c = mat_21 M"
  define d where "d = mat_22 M"
  define g where "g = mat_21 (mat_pow l (mat_minus (mat_int e M) (mat_int f E)))"
  define h where "h = mat_22 (mat_pow l (mat_minus (mat_int e M) (mat_int f E)))"
  from S2 g_def a1_def h_def c1_def have S3: "mat_21 (mat_pow (Suc(l)) (mat_minus (mat_int e M) (mat_int f E))) = g*a1 + h*c1"
    by (metis mat2.exhaust_sel mat_21_mul)
  from mat_22_mul S2 g_def b1_def h_def d1_def have S4: "mat_22 (mat_pow (Suc(l)) (mat_minus (mat_int e M) (mat_int f E))) 
    = g*b1+h*d1" by simp
  have S5: "mat_11 (mat_int e M) = e*a" by (metis a_def mat2.collapse mat2.inject mat_int.simps)
  have S6: "mat_12 (mat_int e M) = e*b" by (metis b_def mat2.collapse mat2.inject mat_int.simps)
  have S7: "mat_21 (mat_int e M) = e*c" by (metis c_def mat2.collapse mat2.inject mat_int.simps)
  have S8: "mat_22 (mat_int e M) = e*d" by (metis d_def mat2.collapse mat2.inject mat_int.simps)
  from a1_def S5 E_def have S9: "a1 = e*a-f" by (metis mat2.collapse mat2.sel(1) mat_int.simps mat_minus.simps 
    mult_cancel_left2 of_nat_1)
  from b1_def S6 E_def have S10: "b1 = e*b" by (smt mat2.collapse mat2.sel(2) mat_int.simps mat_minus.simps mult_eq_0_iff of_nat_0)
  from c1_def S7 E_def have S11: "c1 = e*c" by (smt mat2.collapse mat2.sel(3) mat_int.simps mat_minus.simps mult_cancel_right2 of_nat_0)
  from S11 assms(2) c_def have S115: "c1 = e" by simp
  from d1_def S8 E_def have S12: "d1 = e*d - f" by (metis mat2.collapse mat2.sel(4) mat_int.simps mat_minus.simps mult_cancel_left2 of_nat_1)
  from S12 assms(1) d_def have S125: "d1 = - f" by simp
  from assms(2) c_def Suc g_def c_def have S13: "g mod e^2 = (-1)^(l-1)*l*e*f^(l-1)*c mod e^2" by blast
  from assms(2) c_def S13 have S135: "g mod e^2 = (-1)^(l-1)*l*e*f^(l-1) mod e^2" by simp
  from Suc h_def have S14: "h mod e^2 = (-1)^l *f^l mod e^2" by simp
  from S10 S135 have S27: "g*b1 mod e^2 = (-1)^(l-1)*l*e*f^(l-1)*e*b mod e^2" by (metis mod_mult_left_eq mult.assoc)
  from S27 have S28: "g*b1 mod e^2 = 0 mod e^2" by (simp add: power2_eq_square)
  from S125 S14 mod_mult_cong have S29: "h*d1 mod e^2 = (-1)^l *f^l*(- f) mod e^2" by blast
  from S29 have S30: "h*d1 mod e^2 = (-1)^(l+1) *f^l*f mod e^2" by simp
  from S30 have S31: "h*d1 mod e^2 = (-1)^(l+1) *f^(l+1) mod e^2" by (metis mult.assoc power_add power_one_right)
  from S31 have F2: "?Q (Suc(l))" by (metis S28 S4 Suc_eq_plus1 add.left_neutral mod_add_cong)
  from S9 S13 have S15: "g*a1 mod e^2 = ((-1)^(l-1)*l*e*f^(l-1)*c*(e*a-f))mod e^2" by (metis mod_mult_left_eq)
  have S16: "((-1)^(l-1)*l*e*f^(l-1)*c*(e*a-f)) = ((-1)^(l-1)*l*e^2*f^(l-1)*c*a) - f*(-1)^(l-1)*l*e*f^(l-1)*c" by algebra
  have S17: "((-1)^(l-1)*l*e^2*f^(l-1)*c*a) mod e^2 = 0 mod e^2" by simp
  from S17 have S18: "(((-1)^(l-1)*l*e^2*f^(l-1)*c*a) - f*(-1)^(l-1)*l*e*f^(l-1)*c) mod e^2 =
    - f*(-1)^(l-1)*l*e*f^(l-1)*c mod e^2"
    proof -
      have f1: "\<forall>i ia. (ia::int) - (0 - i) = ia + i"
        by auto
      have "\<forall>i ia. ((0::int) - ia) * i = 0 - ia * i"
        by auto
      then show ?thesis using f1
      proof -
        have f1: "\<And>i. (0::int) - i = - i"
          by presburger
        then have "\<And>i. (i - - ((- 1) ^ (l - 1) * int l * e\<^sup>2 * f ^ (l - 1) * c * a)) mod e\<^sup>2 = i mod e\<^sup>2"
          by (metis (no_types) S17 \<open>\<forall>i ia. ia - (0 - i) = ia + i\<close> add.right_neutral mod_add_right_eq)
        then have "\<And>i. ((- 1) ^ (l - 1) * int l * e\<^sup>2 * f ^ (l - 1) * c * a - i) mod e\<^sup>2 = - i mod e\<^sup>2"
          using f1 by (metis \<open>\<forall>i ia. ia - (0 - i) = ia + i\<close> uminus_add_conv_diff)
        then show ?thesis
          using f1 \<open>\<forall>i ia. (0 - ia) * i = 0 - ia * i\<close> by presburger
      qed 
  qed 
  from S15 S16 S18 have S19: "g*a1 mod e^2 = - f*(-1)^(l-1)*l*e*f^(l-1)*c mod e^2" by presburger
  from S11 S14 have S20: "h*c1 mod e^2 = (-1)^l *f^l*e*c mod e^2" by (metis mod_mult_left_eq mult.assoc)
  from S19 S20 have S21: "(g*a1 + h*c1) mod e^2 = (- f*(-1)^(l-1)*l*e*f^(l-1)*c + (-1)^l *f^l*e*c) mod e^2" using mod_add_cong by blast
  from assms(2) c_def have S22: "(- f*(-1)^(l-1)*l*e*f^(l-1)*c + (-1)^l *f^l*e*c) mod e^2=(- f*(-1)^(l-1)*l*e*f^(l-1) + (-1)^l *f^l*e) mod e^2" by simp
  have S23: "(- f*(-1)^(l-1)*l*e*f^(l-1) + (-1)^l *f^l*e) mod e^2 = (f*(-1)^(l)*l*e*f^(l-1) + (-1)^l *f^l*e) mod e^2" 
    by (smt One_nat_def Suc_pred mult.commute mult_cancel_left2 mult_minus_left neq0_conv of_nat_eq_0_iff power.simps(2))
  have S24: "(f*(-1)^(l)*l*e*f^(l-1) + (-1)^l *f^l*e) mod e^2 = ((-1)^(l)*l*e*f^l + (-1)^l *f^l*e) mod e^2" 
    by (smt One_nat_def Suc_pred mult.assoc mult.commute mult_eq_0_iff neq0_conv of_nat_eq_0_iff power.simps(2))
  have S25: "((-1)^(l)*l*e*f^l + (-1)^l *f^l*e) mod e^2 = ((-1)^(l)*(l+1)*e*f^l) mod e^2"
  proof -
    have f1: "\<forall>i ia. (ia::int) * i = i * ia"
      by simp
    then have f2: "\<forall>i ia. (ia::int) * i - - i = i * (ia - - 1)"
      by (metis (no_types) mult.right_neutral mult_minus_left right_diff_distrib')
    have "\<forall>n. int n - - 1 = int (n + 1)"
      by simp
    then have "e * (f ^ l * (int l * (- 1) ^ l - - ((- 1) ^ l))) mod e\<^sup>2 = e * (f ^ l * ((- 1) ^ l * int (l + 1))) mod e\<^sup>2"
      using f2 by presburger
    then have "((- 1) ^ l * int l * e * f ^ l - - ((- 1) ^ l) * f ^ l * e) mod e\<^sup>2 = (- 1) ^ l * int (l + 1) * e * f ^ l mod e\<^sup>2"
      using f1
    proof -
      have f1: "\<And>i ia ib. (i::int) * (ia * ib) = ia * (i * ib)"
        by simp
      then have "\<And>i ia ib. (i::int) * (ia * ib) - - (i * ib) = (ia - - 1) * (i * ib)"
        by (metis (no_types) \<open>\<forall>i ia. ia * i = i * ia\<close> f2)
      then show ?thesis
        using f1 by (metis (no_types) \<open>\<forall>i ia. ia * i = i * ia\<close> \<open>e * (f ^ l * (int l * (- 1) ^ l - - ((- 1) ^ l))) mod e\<^sup>2 = e * (f ^ l * ((- 1) ^ l * int (l + 1))) mod e\<^sup>2\<close> f2 mult_minus_right)
    qed 
    then show ?thesis
      by simp
  qed
  from S21 S22 S23 S24 S25 have S26: "(g*a1 + h*c1) mod e^2 = ((-1)^(l)*(l+1)*e*f^l) mod e^2" by presburger
  from S3 S26 have F1: "?P (Suc(l))" by (metis Suc_eq_plus1 assms(2) diff_Suc_1 mult.right_neutral)
  from F1 F2 show ?case by simp
qed

lemma divisibility_congruence:
  assumes "m = k*l" and "b>2" "m>0"
  shows "alpha b m mod (alpha b k)^2 = ((-1)^(l-1)*l*(alpha b k)*(alpha b (k-1))^(l-1)) mod (alpha b k)^2"
proof -
  have S0: "alpha b m = mat_21 (A b m)" by (metis A.elims assms(3) mat2.sel(3) neq0_conv)
  from assms S0 divisibility_equations have S1: "alpha b m = 
    mat_21 (mat_pow l (mat_minus (mat_int (alpha b k) (B b)) (mat_int (alpha b (k-1)) (mat 1 0 0 1))))" by auto
  have S2: "mat_21 (B b) = 1" by simp
  have S3: "mat_22 (B b) = 0" by simp
  from S1 S2 S3 divisibility_cong show ?thesis by (metis (no_types, hide_lams) mult.right_neutral of_nat_0 of_nat_1) 
qed

(* Main result section 3.5 *)
theorem divisibility_alpha2:
assumes assms: "m = k*l" and "b>2" "m>0"
shows "(alpha b k)^2 dvd (alpha b m) \<longleftrightarrow> k*(alpha b k) dvd m  " (is "?P \<longleftrightarrow> ?Q")
proof
assume Q: "?Q"
from Q assms have S0: "l mod alpha b k = 0" by simp
from S0 have S1: "l*(alpha b k) mod (alpha b k)^2 = 0" by (simp add: power2_eq_square)
from S1 have S2: "((-1)^(l-1)*l*(alpha b k)*(alpha b (k-1))^(l-1)) mod (alpha b k)^2 = 0"
  proof -
    have "\<forall>i. alpha b k * (int l * i) mod (alpha b k)\<^sup>2 = 0"
      by (metis (no_types) S1 mod_0 mod_mult_left_eq mult.assoc mult.left_commute mult_zero_left)
    then show ?thesis
      by (simp add: mult.assoc mult.left_commute)
  qed
from assms divisibility_congruence have S3: "alpha b m mod (alpha b k)^2 = ((-1)^(l-1)*l*(alpha b k)*(alpha b (k-1))^(l-1)) mod (alpha b k)^2" by simp
from S2 S3 have S4: "alpha b m mod (alpha b k)^2 = 0" by linarith
from S4 dvd_eq_mod_eq_0 show "?P" by auto 
next assume P: "?P"
  from assms divisibility_congruence have S0: "alpha b m mod (alpha b k)^2 = ((-1)^(l-1)*l*(alpha b k)*(alpha b (k-1))^(l-1)) mod (alpha b k)^2" by simp
  from S0 P have S1: "0 = ((-1)^(l-1)*l*(alpha b k)*(alpha b (k-1))^(l-1)) mod (alpha b k)^2" by auto
  from S1 have S2: "0 = l*(alpha b k)*(alpha b (k-1))^(l-1) mod (alpha b k)^2" 
    by (metis (no_types, hide_lams) left_minus_one_mult_self mod_mult_right_eq mult.assoc mult_zero_right)
  from S2 have S3: "l*(alpha b (k-1))^(l-1) mod (alpha b k) = 0"
  proof -
    have f1: "alpha b k * int l * alpha b (k - Suc 0) ^ (l - Suc 0) mod (alpha b k)\<^sup>2 = 0" by (simp add: S2 semiring_normalization_rules(7))
    have f2: "\<And>n na. n * na \<noteq> n \<or> n = 0 \<or> na = Suc 0" by (metis One_nat_def mult_eq_self_implies_10)
    have f3: "alpha b k * (int l * alpha b (k - Suc 0) ^ (l - Suc 0) mod alpha b k) = 0" using f1 by (metis (no_types) mult.assoc mult_mod_right power2_eq_square)
    have "0 \<noteq> m" by (metis assms(3) nat_neq_iff)
    then have f4: "0 \<noteq> k" by (simp add: assms(1))
    then have f5: "m \<noteq> k \<or> Suc 0 = l" using assms(1) by auto
    have f6: "\<And>n na. (n::nat) * na mod n = 0 mod n" by simp
    have f7: "\<And>n na. (n::nat) * na mod n = 0" by simp
    obtain nn :: "nat \<Rightarrow> nat \<Rightarrow> nat" where 
      f8: "\<And>n na nb. \<not> 0 < n \<or> \<not> 2 < na \<or> \<not> alpha na n dvd alpha na nb \<or> n * nn n nb = nb" by (metis (no_types) mkl)
    { assume "k \<noteq> 1"
      moreover
      { assume "1 < k"
        moreover
        { assume "1 < k \<and> (0::nat) \<noteq> 1"
          moreover
          { assume "1 mod k \<noteq> 0"
            moreover
            { assume "k * (k * nn (k * k) k) \<noteq> k"
              then have "k * k * nn (k * k) k \<noteq> k" by (metis mult.assoc)
              then have "\<exists>n>0. n * nn n k \<noteq> k" by (metis (no_types) assms(1) assms(3) nat_0_less_mult_iff) }
            ultimately have "Suc 0 = l \<and> l = 1 \<longrightarrow> (\<exists>n>0. n * nn n k \<noteq> k)" using f7 f4 f2 by (metis (no_types)) }
          ultimately have "m = k \<and> Suc 0 = l \<and> l = 1 \<longrightarrow> (\<exists>n>0. n * nn n k \<noteq> k)" by (metis (no_types) Divides.mod_less) }
        ultimately have "m = k \<and> Suc 0 = l \<and> l = 1 \<longrightarrow> (\<exists>n>0. n * nn n k \<noteq> k)" using nat_neq_iff by blast }
      ultimately have "m = k \<and> Suc 0 = l \<and> l = 1 \<longrightarrow> (\<exists>n>0. n * nn n k \<noteq> k)"
        using f6 by (metis (no_types) Divides.mod_less assms(1) assms(3) nat_0_less_mult_iff nat_neq_iff) }
    then have "alpha b k = 0 \<and> m = k \<longrightarrow> int l * alpha b (k - Suc 0) ^ (l - Suc 0) mod alpha b k = 0 
      \<or> (\<exists>i. (0::int) * i \<noteq> 0) \<or> (\<exists>n>0. n * nn n k \<noteq> k)" using f5 by fastforce
    then have "alpha b k = 0 \<longrightarrow> int l * alpha b (k - Suc 0) ^ (l - Suc 0) mod alpha b k = 0 \<or> (\<exists>i. (0::int) * i \<noteq> 0)"
      using f8 f4 by (metis (no_types) Divides.mod_less add.left_neutral assms(1) assms(2) assms(3) dvd_triv_left mod_mult_self2 nat_0_less_mult_iff nat_neq_iff semiring_normalization_rules(7))
    then show ?thesis using f3 by (metis One_nat_def mult_eq_0_iff)
  qed 
  from div_coprime Suc_eq_plus1 Suc_pred' assms(1) assms(2) assms(3) gcd.commute less_imp_le_nat nat_0_less_mult_iff 
    have S4: "coprime (alpha b k) (alpha b (k-1))" by metis
  from S3 S4 have H2: "l mod (alpha b k) = 0" by (meson coprime_dvd_mult coprime_exp dvd_imp_mod_0 mod_0_imp_dvd)
  from H2 assms(1) show "?Q" by auto
qed

(*    Section 3.6  *)

(*3.6 Congruence Properties*)

(* matrix representations*)
(* j positive: *)
lemma congruence_jpos:
  fixes b m j l :: nat
  assumes "b>2" and "2*l*m+j>0"
  defines "n \<equiv> 2*l*m+j"
  shows "A b n = mat_mul (mat_pow l (mat_pow 2 (A b m))) (A b j)"
proof-
  from A_pow assms(1) have Abm2: "A b n = mat_pow n (B b)" by simp
  from Abm2 n_def have Bn: "mat_pow n (B b) =mat_pow (2*l*m+j) (B b)" by simp
  from mat_exp_law have as1: "mat_pow (2*l*m+j) (B b) = mat_mul (mat_pow l (mat_pow m (mat_pow 2 (B b)))) (mat_pow (j) (B b))" 
    by (metis (no_types, lifting) mat_exp_law_mult mult.commute)
  from A_pow assms(1) B.elims mult.commute mat_exp_law_mult have as2: "mat_mul (mat_pow l (mat_pow m (mat_pow 2 (B b)))) (mat_pow (j) (B b)) 
    = mat_mul (mat_pow l (mat_pow 2 (A b m))) (A b j)" by metis
  from as2 as1 Abm2 Bn show ?thesis by auto
qed

(* j negative is more complicated....
  requires 2 definitions and 3 lemmas  *)

(* definitions of inverse matrices *)
fun A_inv :: "nat \<Rightarrow> nat \<Rightarrow> mat2"
  where
    "A_inv b n = mat (-alpha b (n-1)) (alpha b n) (-alpha b n) (alpha b (n+1))"

fun B_inv :: "nat \<Rightarrow> mat2"
  where
    "B_inv b = mat 0 1 (-1) b"

(* three lemmas *)
lemma congruence_inverse:
  fixes b n :: nat
  assumes "b>2"
  shows "mat_pow (n+1) (B_inv b) = A_inv b (n+1)"
proof(induct n)
  case 0
  then show ?case by simp
next
  case (Suc n)
  then show ?case by simp
qed

lemma congruence_inverse2:
  fixes n b :: nat
  assumes "b>2"
  shows "mat_mul (mat_pow n (B b)) (mat_pow n (B_inv b)) = mat 1 0 0 1"
proof(induct n)
  case 0
  then show ?case by simp
next
  case (Suc n)
  have S1: "mat_pow (Suc(n)) (B b) = mat_mul (B b) (mat_pow n (B b))" by simp
  have S2: "mat_pow (Suc(n)) (B_inv b) = mat_mul (mat_pow n (B_inv b)) (B_inv b)"
    proof -
    have "\<forall>i ia ib ic. mat_pow 1 (mat ic ib ia i) = mat ic ib ia i"
      by simp
    then have "\<forall>m ma mb. mat_pow 1 m = m \<or> mat_mul mb m \<noteq> ma" by (metis mat2.exhaust)
    then show ?thesis
      by (metis (no_types) One_nat_def add_Suc_right diff_Suc_Suc diff_zero mat_exp_law mat_pow.simps(1) mat_pow.simps(2))
  qed
  define "C" where "C= (B b)"
  define "D" where "D = mat_pow n C"
  define "E" where "E = B_inv b"
  define "F" where "F = mat_pow n E"
  from S1 S2 C_def D_def E_def F_def have S3: "mat_mul (mat_pow (Suc(n)) C) (mat_pow (Suc(n)) E) = mat_mul (mat_mul C D) (mat_mul F E)" by simp
  from S3 mat_associativity mat2.exhaust C_def D_def E_def F_def have S4: "mat_mul (mat_pow (Suc(n)) C) (mat_pow (Suc(n)) E) 
    = mat_mul C (mat_mul (mat_mul D F) E)" by metis
  from S4 Suc.hyps mat_neutral_element C_def D_def E_def F_def have S5: "mat_mul (mat_pow (Suc(n)) C) (mat_pow (Suc(n)) E) = mat_mul C E" by simp
  from S5 C_def E_def show ?case by simp
qed

lemma congruence_mult:
  fixes m :: nat
  assumes "b>2"
  shows "n>m ==> mat_pow (nat(int n- int m)) (B b) = mat_mul (mat_pow n (B b)) (mat_pow m (B_inv b))"
proof(induction n)
  case 0
  then show ?case by simp
next
  case (Suc n)
  consider (eqm) "n == m" | (gm) "n < m" | (lm) "n>m" by linarith
  then show ?case
  proof cases
    case gm
    from Suc.prems gm not_less_eq show ?thesis by simp
  next case lm
    have S1: "mat_pow (nat(int (Suc(n)) - int m)) (B b) = mat_mul (B b) (mat_pow (nat(int n - int m)) (B b))" 
      by (metis Suc.prems Suc_diff_Suc Suc_eq_plus1 add.commute assms diff_Suc_Suc mat_exp_law nat_minus_as_int pow_one)
    from lm S1 Suc.IH have S2: "mat_pow (nat(int (Suc(n)) - int m)) (B b) = mat_mul (B b) (mat_mul (mat_pow n (B b)) (mat_pow m (B_inv b)))" by simp
    from S2 mat_associativity mat2.exhaust have S3: "mat_pow (nat(int (Suc(n)) - int m)) (B b) = mat_mul (mat_mul (B b) (mat_pow n (B b))) (mat_pow m (B_inv b))" by metis  
    from S3 show ?thesis by simp
  next case eqm
    from eqm have S1: "nat(int (Suc(n))- int m) = 1" by auto
    from S1 have S2: "mat_pow (nat(int (Suc(n))- int m)) (B b) == B b" by simp
    from eqm have S3: "(mat_pow (Suc(n)) (B b)) = mat_mul (B b) (mat_pow m (B b))" by simp
    from S3 have S35: "mat_mul (mat_pow (Suc(n)) (B b)) (mat_pow m (B_inv b)) = mat_mul (mat_mul (B b) (mat_pow m (B b))) (mat_pow m (B_inv b))" by simp
    from mat2.exhaust S35 mat_associativity have S4: "mat_mul (mat_pow (Suc(n)) (B b)) (mat_pow m (B_inv b)) 
      = mat_mul (B b) (mat_mul (mat_pow m (B b)) (mat_pow m (B_inv b)))" by smt 
    from congruence_inverse2 assms have S5: "mat_mul (mat_pow m (B b)) (mat_pow m (B_inv b)) =  mat 1 0 0 1" by simp
    have S6: "mat_mul (B b) (B_inv b) = mat 1 0 0 1" by simp
    from S5 S6 eqm have S7: "mat_mul (mat_pow n (B b)) (mat_pow m (B_inv b)) = mat 1 0 0 1" by metis
    from S7 have S8: "mat_mul (B b) (mat_mul (mat_pow n (B b)) (mat_pow m (B_inv b))) == B b" by simp
    from eqm S2 S4 S8 show ?thesis by simp
  qed
qed

(* j negative *)
lemma congruence_jneg:
  fixes b m j l :: nat
  assumes "b>2" and "2*l*m > j" and "j>=1"
  defines "n \<equiv> nat(int 2*l*m- int j)"
  shows "A b n = mat_mul (mat_pow l (mat_pow 2 (A b m))) (A_inv b j)"
proof-
  from A_pow assms(1) have Abm2: "A b n = mat_pow n (B b)" by simp
  from Abm2 n_def have Bn: "A b n = mat_pow (nat(int 2*l*m- int j)) (B b)" by simp
  from Bn congruence_mult assms(1) assms(2) have Bn2: "A b n = mat_mul (mat_pow (2*l*m) (B b)) (mat_pow j (B_inv b))" by fastforce
  from assms(1) assms(3) congruence_inverse Bn2 add.commute le_Suc_ex have Bn3: "A b n = mat_mul (mat_pow (2*l*m) (B b)) (A_inv b j)" by smt
  from Bn3 A_pow assms(1) mult.commute B.simps mat_exp_law_mult have as3: "A b n = mat_mul (mat_pow l (mat_pow 2 (A b m))) (A_inv b j)" by metis
  from as3 A_pow add.commute assms(1) mat_exp_law mat_exp_law_mult show ?thesis by simp
qed

(* lemma for element-wise congruence*)
lemma matrix_congruence:
  fixes Y Z :: mat2
  fixes b m j l :: nat
  assumes "b>2"
  defines "X \<equiv> mat_mul Y Z"
  defines "a \<equiv> mat_11 Y" and "b0\<equiv> mat_12 Y" and "c \<equiv> mat_21 Y" and "d \<equiv> mat_22 Y"
  defines "e \<equiv> mat_11 Z" and "f \<equiv> mat_12 Z" and "g \<equiv> mat_21 Z" and "h \<equiv> mat_22 Z"
  defines "v \<equiv> alpha b (m+1) - alpha b (m-1)"
  assumes "a mod v = a1 mod v" and "b0 mod v = b1 mod v" and "c mod v = c1 mod v" and "d mod v = d1 mod v"
  shows "mat_21 X mod v = (c1*e+d1*g) mod v \<and> mat_22 X mod v = (c1*f+ d1*h) mod v" (is "?P \<and> ?Q")
proof -
  (* proof of ?P *)
  from X_def mat2.exhaust_sel mat_21_mul c_def e_def d_def g_def have P1: "mat_21 X = (c*e+d*g)" by metis
  from assms(14) mod_mult_cong have P2: "(c*e) mod v = (c1*e) mod v" by blast 
  from assms(15) mod_mult_cong have P3: "(d*g) mod v = (d1*g) mod v" by blast
  from P2 P3  mod_add_cong have P4: "(c*e+d*g) mod v = (c1*e+d1*g) mod v" by blast
  from P1 P4 have F1: ?P by simp
  (* proof of ?Q *)
  from X_def mat2.exhaust_sel mat_21_mul c_def f_def d_def h_def mat2.sel(4) mat_mul.simps have Q1: "mat_22 X = (c*f+d*h)" by metis
  from assms(14) mod_mult_cong have Q2: "(c*f) mod v = (c1*f) mod v" by blast 
  from assms(15) mod_mult_cong have Q3: "(d*h) mod v = (d1*h) mod v" by blast
  from Q1 Q2 Q3 mod_add_cong have F2: ?Q by fastforce
  from F1 F2 show ?thesis by auto 
qed

(* 3.38 *)
lemma congruence_Abm:
  fixes b m n :: nat
  assumes "b>2"
  defines "v \<equiv> alpha b (m+1) - alpha b (m-1)"
  shows "mat_21 (mat_pow n (mat_pow 2 (A b m))) mod v = 0 mod v 
  \<and> mat_22 (mat_pow n (mat_pow 2 (A b m))) mod v = ((-1)^n) mod v" (is "?P n \<and> ?Q n")
proof(induct n)
case 0
  from mat2.exhaust have S1: "mat_pow 0 (mat_pow 2 (A b m)) =  mat 1 0 0 1" by simp
  then show ?case by simp
next
  case (Suc n)
  define Z where "Z = mat_pow 2 (A b m)"
  define Y where "Y = mat_pow n Z"
  define X where "X = mat_mul Y Z"
  define c where "c = mat_21 Y"
  define d where "d = mat_22 Y"
  define e where "e = mat_11 Z"
  define f where "f = mat_12 Z"
  define g where "g = mat_21 Z"
  define h where "h = mat_22 Z"
  define d1 where "d1 = (-1)^n mod v"
  from d_def d1_def Z_def Y_def Suc.hyps have S1: "d mod v = d1 mod v" by simp
  from matrix_congruence assms(1) X_def v_def c_def d_def e_def d1_def g_def S1 have S2: "mat_21 X mod v = (c*e+d1*g) mod v" by blast
  from Z_def Y_def c_def Suc.hyps have S3: "c mod v = 0 mod v" by simp
  consider (eq0) "m = 0" | (g0) "m>0" by blast
  then have S4: "g mod v = 0"
  proof cases
    case eq0
    from eq0 have S1: "A b m = mat 1 0 0 1" by simp
    from S1 Z_def div_3252 g_def show ?thesis by simp
    next
    case g0
    from g0 A.elims neq0_conv have S1: "A b m = mat (alpha b (m + 1)) (-(alpha b m)) (alpha b m) (-(alpha b (m - 1)))" by metis
    from S1 A_pow assms(1) mat2.sel(3) mat_exp_law mat_exp_law_mult mat_mul.simps mult_2 
      have S2: "mat_21 (mat_pow 2 (A b m)) = (alpha b m)*(alpha b (m+1)) + (-alpha b (m-1))*(alpha b m)" by smt
    from S2 g_def Z_def g0 A.elims neq0_conv have S3: "g = (alpha b (m+1))*(alpha b m)- (alpha b m)*(alpha b (m-1))" by simp
    from S3 g_def v_def mod_mult_self1_is_0 mult.commute right_diff_distrib show ?thesis by metis
  qed
  from S2 S3 S4 Z_def div_3252 g_def mat2.exhaust_sel mod_0 have F1: "?P (Suc(n))" by metis
  (* Now proof of Q *)
  from d_def d1_def Z_def Y_def Suc.hyps have Q1: "d mod v = d1 mod v" by simp
  from matrix_congruence assms(1) X_def v_def c_def d_def f_def d1_def h_def S1 have Q2: "mat_22 X mod v = (c*f+d1*h) mod v" by blast
  from Z_def Y_def c_def Suc.hyps have Q3: "c mod v = 0 mod v" by simp
  consider (eq0) "m = 0" | (g0) "m>0" by blast
  then have Q4: "h mod v = (-1) mod v"
  proof cases
    case eq0
    from eq0 have S1: "A b m = mat 1 0 0 1" by simp
    from eq0 v_def have S2: "v = 1" by simp
    from S1 S2 show ?thesis by simp
    next
    case g0
    from g0 A.elims neq0_conv have S1: "A b m = mat (alpha b (m + 1)) (-(alpha b m)) (alpha b m) (-(alpha b (m - 1)))" by metis
    from S1 A_pow assms(1) mat2.sel(4) mat_exp_law mat_exp_law_mult mat_mul.simps mult_2 
      have S2: "mat_22 (mat_pow 2 (A b m)) = (alpha b m)*(-(alpha b m)) + (-(alpha b (m - 1)))*(-(alpha b (m - 1)))" by smt
    from S2 Z_def h_def have S3: "h = -(alpha b m)*(alpha b m) + (alpha b (m - 1))*(alpha b (m - 1))" by simp
    from v_def add.commute diff_add_cancel mod_add_self2 have S4: "(alpha b (m - 1)) mod v = alpha b (m+1) mod v" by metis
    from S3 S4 mod_diff_cong mod_mult_left_eq mult.commute mult_minus_right uminus_add_conv_diff 
      have S5: "h mod v = (-(alpha b m)*(alpha b m) + (alpha b (m - 1))*(alpha b (m + 1))) mod v" by metis
    from One_nat_def add.right_neutral add_Suc_right alpha.elims diff_Suc_1 g0 le_imp_less_Suc le_simps(1) neq0_conv alpha_n Suc_diff_1 
      have S6: "alpha b (m + 1) = b* (alpha b m)- alpha b (m-1)" by metis
    from S6 have S7: "(alpha b (m - 1))*(alpha b (m + 1)) = (int b) * (alpha b (m-1) * (alpha b m)) - (alpha b (m-1))^2"
    proof -
      have f1: "\<forall>i ia. - ((ia::int) * i) = ia * - i" by simp
      have "\<forall>i ia ib ic. (ic::int) * (ib * ia) + ib * i = ib * (ic * ia + i)" by (simp add: distrib_left)
      then show ?thesis using f1 by (metis S6 ab_group_add_class.ab_diff_conv_add_uminus power2_eq_square)
    qed 
    from S7 have S8: "(-(alpha b m)*(alpha b m) + (alpha b (m - 1))*(alpha b (m + 1))) 
      = -1*(alpha b (m-1))^2 + (int b) * (alpha b (m-1) * (alpha b m)) - (alpha b m)^2" by (simp add: power2_eq_square)
    from alpha_det2  assms(1) g0 have S9: "-1*(alpha b (m-1))^2 + (int b) * (alpha b (m-1) * (alpha b m)) - (alpha b m)^2 = -1" by smt
    from S5 S8 S9 show ?thesis by simp
  qed
  from Q2 Q3 Q4 Suc_eq_plus1 add.commute add.right_neutral d1_def mod_add_right_eq mod_mult_left_eq mod_mult_right_eq mult.right_neutral 
    mult_minus1 mult_minus_right mult_zero_left power_Suc have Q5: "mat_22 X mod v = (-1)^(n+1) mod v" by metis
  from Q5 Suc_eq_plus1 X_def Y_def Z_def mat_exp_law mat_exp_law_mult mult.commute mult_2 one_add_one have F2: "?Q (Suc(n))" by metis
  from F1 F2 show ?case by blast
qed

(* main result section 3.6: 3.36 part 1*)
lemma congruence_main:
  fixes b m j l :: nat
  assumes "b>2"
  defines "n \<equiv> 2*l*m + j"
  defines "v \<equiv> alpha b (m+1) - alpha b (m-1)"
  shows "(alpha b n) mod v = ((-1)^l * alpha b j) mod v"
proof -
  define Y where "Y = mat_pow l (mat_pow 2 (A b m))"
  define Z where "Z = A b j"
  define X where "X = mat_mul Y Z"
  define c where "c = mat_21 Y"
  define d where "d = mat_22 Y"
  define e where "e = mat_11 Z"
  define g where "g = mat_21 Z"
  define d1 where "d1 = (-1)^l mod v"
  from congruence_Abm assms(1) d_def v_def Y_def d1_def have S0: "d mod v = d1 mod v" by simp_all
  from matrix_congruence assms(1) X_def v_def c_def d_def e_def d1_def g_def S0 have S1: "mat_21 X mod v = (c*e+d1*g) mod v" by blast
  from congruence_Abm d1_def v_def mod_mod_trivial have S2: "d1 mod v = (-1)^l mod v" by blast
  from congruence_Abm Y_def assms(1) c_def v_def have S3: "c mod v = 0" by simp
  from Z_def g_def A.elims alpha.simps(1) mat2.sel(3) mat2.exhaust have S4: "g = alpha b j" by metis
  from A_pow assms(1) mat_exp_law mat_exp_law_mult mult_2 mult_2_right n_def X_def Y_def Z_def have S5: "A b n = X" by metis
  from S5 A.elims alpha.simps(1) mat2.sel(3) Z_def Y_def have S6: "mat_21 X = alpha b n" by metis
  from S2 S3 S4 S6 S1 add.commute mod_0 mod_mult_left_eq mod_mult_self2 mult_zero_left zmod_eq_0_iff show ?thesis by metis
qed