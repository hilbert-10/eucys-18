                        HILBERT MEETS ISABELL
         Formalization of Hilbert's Tenth Problem in Isabelle


                Marco David, Abhik Pal, Benedikt Stock

      European Union Contest for Young Scientists, 2018 (Dublin)

                                 ~~~

1. OVERVIEW OF FILES

   - code -- our contributions to the Hilbert 10 formalization.
   - report/* -- the main report for the project.


2. LICENSE

    - Report: Copyright 2018 Marco David, Abhik Pal, Benedikt Stock
    - Code: GNU Public License v3 (GPL v3) (see code/GPL.txt for
          details)
